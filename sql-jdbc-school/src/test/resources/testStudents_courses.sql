INSERT INTO public.groups(group_id, group_name) VALUES (DEFAULT, 'TT-44');

INSERT INTO public.students(student_id, group_id, first_name, last_name) VALUES (DEFAULT, 1, 'test1', 'testov');
INSERT INTO public.students(student_id, group_id, first_name, last_name) VALUES (DEFAULT, 1, 'test2', 'testov');
INSERT INTO public.students(student_id, group_id, first_name, last_name) VALUES (DEFAULT, 1, 'test3', 'testov');

INSERT INTO public.courses (course_id, course_name, course_description) VALUES (DEFAULT, 'test1', 'description1');
INSERT INTO public.courses (course_id, course_name, course_description) VALUES (DEFAULT, 'test2', 'description2');
INSERT INTO public.courses (course_id, course_name, course_description) VALUES (DEFAULT, 'test3', 'description3');

INSERT INTO public.students_courses(student_id, course_id) VALUES (1, 1);
INSERT INTO public.students_courses(student_id, course_id) VALUES (1, 3);
INSERT INTO public.students_courses(student_id, course_id) VALUES (2, 1);
INSERT INTO public.students_courses(student_id, course_id) VALUES (3, 1);
