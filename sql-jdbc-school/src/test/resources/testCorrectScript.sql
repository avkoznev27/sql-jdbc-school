-- Table: public.groups
DROP TABLE IF EXISTS public.groups CASCADE;
CREATE TABLE public.groups
(
    group_id SERIAL NOT NULL,
    group_name character varying NOT NULL,
    CONSTRAINT groups_pkey PRIMARY KEY (group_id)
);

-- Table: public.students
DROP TABLE IF EXISTS public.students CASCADE;
CREATE TABLE public.students
(
    student_id SERIAL NOT NULL,
    group_id integer NOT NULL,
    first_name character varying NOT NULL,
    last_name character varying NOT NULL,
    CONSTRAINT students_pkey PRIMARY KEY (student_id),
    CONSTRAINT group_id FOREIGN KEY (group_id) REFERENCES public.groups (group_id)
	);
