INSERT INTO public.groups(group_id, group_name) VALUES (DEFAULT, 'TT-56');
INSERT INTO public.groups(group_id, group_name) VALUES (DEFAULT, 'TT-57');

INSERT INTO public.students(student_id, group_id, first_name, last_name) VALUES (DEFAULT, 1 , 'test1', 'testov1');
INSERT INTO public.students(student_id, group_id, first_name, last_name) VALUES (DEFAULT, 1 , 'test2', 'testov2');
INSERT INTO public.students(student_id, group_id, first_name, last_name) VALUES (DEFAULT, 2 , 'test3', 'testov3');
INSERT INTO public.students(student_id, group_id, first_name, last_name) VALUES (DEFAULT, 2 , 'test4', 'testov4');
INSERT INTO public.students(student_id, group_id, first_name, last_name) VALUES (DEFAULT, 2 , 'test5', 'testov5');
