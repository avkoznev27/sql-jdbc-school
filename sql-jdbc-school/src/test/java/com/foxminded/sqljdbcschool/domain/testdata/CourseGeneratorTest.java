package com.foxminded.sqljdbcschool.domain.testdata;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.foxminded.sqljdbcschool.domain.model.Course;

class CourseGeneratorTest {

    private Generator<Course> courseGen;

    @BeforeEach
    void setUp() throws Exception {
        courseGen = new CourseGenerator();
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> courseGen.generate(null));
    }

    @Test
    void shouldReturnEmptyListWhenZeroPassed() {
        assertEquals(Collections.emptyList(), courseGen.generate(0));
    }

    @Test
    void shouldReturnListCoursesWithPassedQuantity() {
        assertEquals(5, courseGen.generate(5).size());
    }
}
