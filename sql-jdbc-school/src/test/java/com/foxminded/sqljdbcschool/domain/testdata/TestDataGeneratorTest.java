package com.foxminded.sqljdbcschool.domain.testdata;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestDataGeneratorTest {

    private TestDataGenerator testDataGenerator;
    
    @BeforeEach
    void setUp() throws Exception {
        testDataGenerator = new TestDataGenerator();
    }

    @Test
    void shouldReturnTestDataObjectWithListsGroupsAndCoursesOfSizeAccordingToPassedValues() {
        TestData testData = testDataGenerator.generateData(100, 10, 20);
        assertEquals(10, testData.getGroups().size());
        assertEquals(20, testData.getCourses().size());
    }

    @Test
    void shouldReturnTestDataObjectWithListStudentsOfMaxSizePassedValueMinSizeAccordingToRandomDistribution() {
        TestData testData = testDataGenerator.generateData(100, 10, 20);
        assertFalse(testData.getStudents().isEmpty());
    }
}
