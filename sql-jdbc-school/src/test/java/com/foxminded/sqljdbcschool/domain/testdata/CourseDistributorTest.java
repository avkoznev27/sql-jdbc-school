package com.foxminded.sqljdbcschool.domain.testdata;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.foxminded.sqljdbcschool.domain.model.Course;
import com.foxminded.sqljdbcschool.domain.model.Student;

class CourseDistributorTest {

    private Distributor courseDistributor;
    private List<Student> students;
    private List<Course> courses;

    @BeforeEach
    void setUp() throws Exception {
        students = createTestStudentsList(30);
        courses = Arrays.asList(new Course("test1", "description1"), 
                                new Course("test2", "description2"),
                                new Course("test3", "description3"));
        courseDistributor = new CourseDistributor(students, courses);

    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenStudentsListOrCoursesListIsNull() {
        Distributor courseDistributor1 = new CourseDistributor(students, null);
        Distributor courseDistributor2 = new CourseDistributor(null, courses);
        Distributor courseDistributor3 = new CourseDistributor(null, null);
        assertThrows(IllegalArgumentException.class, () -> courseDistributor1.distribute());
        assertThrows(IllegalArgumentException.class, () -> courseDistributor2.distribute());
        assertThrows(IllegalArgumentException.class, () -> courseDistributor3.distribute());
    }

    @Test
    void shouldSetForEachStudentFieldCoursesWhenStudentsListOrCoursesListPassed() {
        courseDistributor.distribute();
        students.forEach(student -> assertFalse(student.getCourses().isEmpty()));
    }

    private List<Student> createTestStudentsList(int numberOfStudents) {
        return Stream.iterate(0, n -> n + 1)
                .limit(numberOfStudents)
                .map(n -> {
            Student student = new Student();
            student.setFirstName("test" + n);
            student.setLastName("testov" + n);
            return student;
        }).collect(Collectors.toList());
    }
}
