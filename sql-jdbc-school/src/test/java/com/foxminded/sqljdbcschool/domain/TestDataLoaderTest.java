package com.foxminded.sqljdbcschool.domain;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import org.mockito.junit.jupiter.MockitoExtension;

import com.foxminded.sqljdbcschool.dao.CourseDAO;
import com.foxminded.sqljdbcschool.dao.GroupDAO;
import com.foxminded.sqljdbcschool.dao.StudentDAO;
import com.foxminded.sqljdbcschool.domain.testdata.TestData;
import com.foxminded.sqljdbcschool.exceptions.DAOException;

@ExtendWith(MockitoExtension.class)
class TestDataLoaderTest {

    private TestDataLoader testDataLoader;

    @Mock
    private GroupDAO groupDAO;
    @Mock
    private CourseDAO courseDAO;
    @Mock
    private StudentDAO studentDAO;
    @Mock
    private TestData testData;

    @BeforeEach
    void setUp() throws Exception {
        testDataLoader = new TestDataLoader();
    }

    @Test
    void testFillTablesShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class,
                () -> testDataLoader.fillTables(null, courseDAO, studentDAO, testData));
        assertThrows(IllegalArgumentException.class,
                () -> testDataLoader.fillTables(groupDAO, null, studentDAO, testData));
        assertThrows(IllegalArgumentException.class,
                () -> testDataLoader.fillTables(groupDAO, courseDAO, null, testData));
        assertThrows(IllegalArgumentException.class,
                () -> testDataLoader.fillTables(groupDAO, courseDAO, studentDAO, null));
    }

    @Test
    void testFillTablesShouldAddTestDataInDataBaseWhenTestDataPassed() throws DAOException {
        testDataLoader.fillTables(groupDAO, courseDAO, studentDAO, testData);
        verify(groupDAO).addAll(testData.getGroups());
        verify(courseDAO).addAll(testData.getCourses());
        verify(studentDAO).addAll(testData.getStudents());
    }
}
