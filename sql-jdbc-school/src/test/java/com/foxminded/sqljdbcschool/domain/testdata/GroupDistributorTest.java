package com.foxminded.sqljdbcschool.domain.testdata;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.foxminded.sqljdbcschool.domain.model.Group;
import com.foxminded.sqljdbcschool.domain.model.Student;

class GroupDistributorTest {

    private Distributor groupsDistributor;
    private Group group;
    private List<Student> students;
    private List<Group> groups;

    @BeforeEach
    void setUp() throws Exception {
        group = new Group("TT-11");
        groups = Collections.singletonList(group);
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenStudentsListOrGroupsListIsNull() {
        groupsDistributor = new GroupDistributor(students, groups);
        assertThrows(IllegalArgumentException.class, () -> groupsDistributor.distribute());
    }
    
    @Test
    void shouldSetStudentsListForGroupWhenStudentsMoreThan10AndLessThan30InGroup() {
        students = createTestStudentsList(10);
        groupsDistributor = new GroupDistributor(students, groups);
        groupsDistributor.distribute();
        assertEquals(10, group.getStudents().size());
    }

    @Test
    void shouldSetGroupForEachStudentWhenStudentsMoreThan10AndLessThan30InGroup() {
        students = createTestStudentsList(29);
        groupsDistributor = new GroupDistributor(students, groups);
        groupsDistributor.distribute();
        students.forEach(student -> assertNotNull(student.getGroup()));
    }
    
    @Test
    void shouldRemoveStudentsWhenStudentsMoreThan30InGroup() {
        students = createTestStudentsList(35);
        groupsDistributor = new GroupDistributor(students, groups);
        groupsDistributor.distribute();
        assertEquals(30, students.size());
    }
    
    @Test
    void shouldRemoveStudentsWhenStudentsLessThan10InGroup() {
        students = createTestStudentsList(9);
        groupsDistributor = new GroupDistributor(students, groups);
        groupsDistributor.distribute();
        assertTrue(students.isEmpty());
    }
    
    @Test
    void shouldRemoveStudentsFromGroupsWhenStudentsLessThan10InGroup() {
        students = createTestStudentsList(9);
        groupsDistributor = new GroupDistributor(students, groups);
        groupsDistributor.distribute();
        assertTrue(group.getStudents().isEmpty());
    }

    private List<Student> createTestStudentsList(int numberOfStudents) {
        return Stream.iterate(0, n -> n + 1)
                .limit(numberOfStudents)
                .map(n -> {
            Student student = new Student();
            student.setFirstName("test" + n);
            student.setLastName("testov" + n);
            return student;
        }).collect(Collectors.toList());
    }
}
