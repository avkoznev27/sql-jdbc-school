package com.foxminded.sqljdbcschool.domain.testdata;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.foxminded.sqljdbcschool.domain.model.Student;

class StudentGeneratorTest {

    private Generator<Student> studentsGen;

    @BeforeEach
    void setUp() throws Exception {
        studentsGen = new StudentGenerator();
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> studentsGen.generate(null));
    }

    @Test
    void shouldReturnEmptyListWhenZeroPassed() {
        assertEquals(Collections.emptyList(), studentsGen.generate(0));
    }

    @Test
    void shouldReturnListStudentsWithPassedQuantity() {
        assertEquals(200, studentsGen.generate(200).size());
    }
}
