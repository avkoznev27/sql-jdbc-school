package com.foxminded.sqljdbcschool.domain;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static java.util.Collections.singletonList;


import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.foxminded.sqljdbcschool.dao.CourseDAO;
import com.foxminded.sqljdbcschool.dao.GroupDAO;
import com.foxminded.sqljdbcschool.dao.StudentDAO;
import com.foxminded.sqljdbcschool.domain.model.Course;
import com.foxminded.sqljdbcschool.domain.model.Group;
import com.foxminded.sqljdbcschool.domain.model.Student;
import com.foxminded.sqljdbcschool.exceptions.DAOException;
import com.foxminded.sqljdbcschool.exceptions.RequestHandlerException;

@ExtendWith(MockitoExtension.class)
class RequestHandlerTest {

    @InjectMocks
    private RequestHandler requestHandler;

    @Mock
    private GroupDAO groupDAO;
    @Mock
    private CourseDAO courseDAO;
    @Mock
    private StudentDAO studentDAO;

    @Test
    void testAddStudentShouldThrowRequestHandlerExceptionWhenDaoThrowsException() throws DAOException {
        doThrow(DAOException.class).when(studentDAO).add(any(Student.class));
        assertThrows(RequestHandlerException.class,
                () -> requestHandler.addStudent(any(Group.class), "test", "testov"));
    }

    @Test
    void testAddStudentShouldAddStudentInDataBaseWhenGroupAndNamePassed() throws DAOException {
        requestHandler.addStudent(any(Group.class), "test", "testov");
        verify(studentDAO).add(any(Student.class));
    }

    @Test
    void testAddStudentToCourseShouldThrowRequestHandlerExceptionWhenDaoThrowsException() throws DAOException {
        doThrow(DAOException.class).when(studentDAO).addToCourse(anyInt(), anyInt());
        assertThrows(RequestHandlerException.class, () -> requestHandler.addStudentToCource(1, 1));
    }

    @Test
    void testAddStudentToCourseShouldAddStudentToCourseWhenStudentIdAndCourseIdPassed() throws DAOException {
        requestHandler.addStudentToCource(anyInt(), anyInt());
        verify(studentDAO).addToCourse(anyInt(), anyInt());
    }

    @Test
    void testGetAllStudentsShouldThrowRequestHandlerExceptionWhenDaoThrowsException() throws DAOException {
        when(studentDAO.getAll()).thenThrow(DAOException.class);
        assertThrows(RequestHandlerException.class, () -> requestHandler.getAllStudents());
    }

    @Test
    void testGetAllStudentsShouldReturnListAllStudentsFromDataBase() throws DAOException {
        List<Student> expected = singletonList(new Student("test", "testov", new Group("TT-11")));
        when(studentDAO.getAll()).thenReturn(expected);
        List<Student> actual = requestHandler.getAllStudents();
        assertEquals(expected, actual);
    }

    @Test
    void testDeleteStudentByIdShouldThrowRequestHandlerExceptionWhenDaoThrowsException() throws DAOException {
        doThrow(DAOException.class).when(studentDAO).delete(anyInt());
        assertThrows(RequestHandlerException.class, () -> requestHandler.deleteStudentById(1));
    }

    @Test
    void testDeleteStudentByIdShouldDeleteStudentWhenStudentIdPassed() throws DAOException {
        requestHandler.deleteStudentById(anyInt());
        verify(studentDAO).delete(anyInt());
    }

    @Test
    void testGetAllStudentsByCourseNameShouldThrowRequestHandlerExceptionWhenDaoThrowsException() throws DAOException {
        when(studentDAO.getAllStudentsFromCourse(anyString())).thenThrow(DAOException.class);
        assertThrows(RequestHandlerException.class, () -> requestHandler.getAllStudentsByCourseName(anyString()));
    }

    @Test
    void testGetAllStudentsByCourseNameShouldReturnStudentsListWhenCourseNamePassed() throws DAOException {
        List<Student> studentsOfCourse = singletonList(new Student("test", "testov", new Group("TT-11")));
        when(studentDAO.getAllStudentsFromCourse(anyString())).thenReturn(studentsOfCourse);
        List<Student> actual = requestHandler.getAllStudentsByCourseName("course");
        assertEquals(studentsOfCourse, actual);
    }

    @Test
    void testGetAllGroupsShouldThrowRequestHandlerExceptionWhenDaoThrowsException() throws DAOException {
        when(groupDAO.getAll()).thenThrow(DAOException.class);
        assertThrows(RequestHandlerException.class, () -> requestHandler.getAllGroups());
    }

    @Test
    void testGetAllGroupsShouldReturnAllGroupListFromDataBase() throws DAOException {
        List<Group> expected = singletonList(new Group("TT-11"));
        when(groupDAO.getAll()).thenReturn(expected);
        List<Group> actual = requestHandler.getAllGroups();
        assertEquals(expected, actual);
    }

    @Test
    void testGetAllGroupsByStudentsShouldThrowRequestHandlerExceptionWhenDaoThrowsException() throws DAOException {
        when(groupDAO.getGroupsByStudents(anyInt())).thenThrow(DAOException.class);
        assertThrows(RequestHandlerException.class, () -> requestHandler.getAllGroupsByStudents(anyInt()));
    }

    @Test
    void testGetAllGroupsShouldReturnGroupListFromDataBase() throws DAOException {
        List<Group> expected = singletonList(new Group("TT-11"));
        when(groupDAO.getGroupsByStudents(anyInt())).thenReturn(expected);
        List<Group> actual = requestHandler.getAllGroupsByStudents(anyInt());
        assertEquals(expected, actual);
    }

    @Test
    void testGetAllCoursesShouldThrowRequestHandlerExceptionWhenDaoThrowsException() throws DAOException {
        when(courseDAO.getAll()).thenThrow(DAOException.class);
        assertThrows(RequestHandlerException.class, () -> requestHandler.getAllCourses());
    }

    @Test
    void testGetAllCoursesShouldReturnAllCoursesListFromDataBase() throws DAOException {
        List<Course> expected = singletonList(new Course("test", "description"));
        when(courseDAO.getAll()).thenReturn(expected);
        List<Course> actual = requestHandler.getAllCourses();
        assertEquals(expected, actual);
    }

    @Test
    void testGetAllCoursesForStudentShouldThrowRequestHandlerExceptionWhenDaoThrowsException() throws DAOException {
        when(courseDAO.getAllCoursesByStudent(anyInt())).thenThrow(DAOException.class);
        assertThrows(RequestHandlerException.class, () -> requestHandler.getAllCoursesForStudent(anyInt()));
    }

    @Test
    void testGetAllCoursesForStudentShouldReturnACoursesListWhenStudentIdPassed() throws DAOException {
        List<Course> expected = singletonList(new Course("test", "description"));
        when(courseDAO.getAllCoursesByStudent(anyInt())).thenReturn(expected);
        List<Course> actual = requestHandler.getAllCoursesForStudent(anyInt());
        assertEquals(expected, actual);
    }

    @Test
    void testRemoveStudentFromCourseShouldThrowRequestHandlerExceptionWhenDaoThrowsException() throws DAOException {
        doThrow(DAOException.class).when(studentDAO).removeFromCourse(anyInt(), anyInt());
        assertThrows(RequestHandlerException.class, () -> requestHandler.removeStudentFromCourse(anyInt(), anyInt()));
    }

    @Test
    void testRemoveStudentFromCourseShouldDeleteStudentWhenStudentIdAndCourseIdPassed() throws DAOException {
        requestHandler.removeStudentFromCourse(anyInt(), anyInt());
        verify(studentDAO).removeFromCourse(anyInt(), anyInt());
    }
}
