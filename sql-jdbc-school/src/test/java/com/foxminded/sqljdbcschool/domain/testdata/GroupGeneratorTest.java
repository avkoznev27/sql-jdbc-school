package com.foxminded.sqljdbcschool.domain.testdata;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.foxminded.sqljdbcschool.domain.model.Group;

class GroupGeneratorTest {

    private Generator<Group> groupGen;

    @BeforeEach
    void setUp() throws Exception {
        groupGen = new GroupGenerator();
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> groupGen.generate(null));
    }

    @Test
    void shouldReturnEmptyListWhenZeroPassed() {
        assertEquals(Collections.emptyList(), groupGen.generate(0));
    }

    @Test
    void shouldReturnListGroupsWithPassedQuantity() {
        assertEquals(5, groupGen.generate(5).size());
    }
}
