package com.foxminded.sqljdbcschool.domain.testdata;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.foxminded.sqljdbcschool.exceptions.FileReaderException;


class FileReaderTest {

    private FileReader fileReader;
    private List<String> testList;

    @BeforeEach
    void setUp() throws Exception {
        fileReader = new FileReader();
    }

    @Test
    void shouldThrowFileReaderExeptionWhenNameNonExistentFileOrNullPassed() {
        assertThrows(FileReaderException.class, () -> fileReader.read(null));
        assertThrows(FileReaderException.class, () -> fileReader.read("nonex"));
    }

    @Test
    void shouldThrowFileReaderExeptionWhenEmtyFilePassed() {
        assertThrows(FileReaderException.class, () -> fileReader.read("testEmpty.txt"));
    }

    @Test
    void shouldReturnListAllLinesFileWhenNotEmtyFilePassed() {
        testList = fileReader.read("testNonEmpty.txt");
        assertNotNull(testList);
    }
}
