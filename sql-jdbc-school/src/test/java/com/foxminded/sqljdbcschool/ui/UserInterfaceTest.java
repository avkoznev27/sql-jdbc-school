package com.foxminded.sqljdbcschool.ui;

import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.junit.jupiter.MockitoExtension;

import com.foxminded.sqljdbcschool.domain.RequestHandler;
import com.foxminded.sqljdbcschool.domain.model.Course;
import com.foxminded.sqljdbcschool.domain.model.Group;
import com.foxminded.sqljdbcschool.domain.model.Student;

@ExtendWith(MockitoExtension.class)
class UserInterfaceTest {

    private static final String EXIT = "exit";
    private static final String BACK = "back";
    private static final String WRONG_INPUT = "wrong input";

    @InjectMocks
    private UserInterface userInterface;

    @Mock
    private RequestHandler requestHandler;
    @Mock
    private InputReader inputReader;
    @Mock
    private View view;

    @Test
    void testStartDialogWhenWrongInput() {
        when(inputReader.readInput())
                                    .thenReturn("asdf")
                                    .thenReturn(EXIT);
        userInterface.startDialog();
        verify(view, atLeast(1)).printWelcome();
        verify(view, atLeast(1)).printAvailableRequests();
        verify(view, atLeast(1)).printWrongInput();
    }

    @Test
    void testStartDialogWhenInputRequest1() {
        String request = "1";
        String quantityStudent = "20";
        when(inputReader.readInput())
                                    .thenReturn(request)
                                    .thenReturn(quantityStudent)
                                    .thenReturn(WRONG_INPUT)
                                    .thenReturn(BACK)
                                    .thenReturn(EXIT);
        userInterface.startDialog();
        verify(view, atLeast(1)).printWelcome();
        verify(view, atLeast(1)).printAvailableRequests();
        verify(view, atLeast(1)).printEnterQuantityStudents();
        verify(requestHandler, atLeast(1)).getAllGroupsByStudents(anyInt());
        verify(view, atLeast(1)).printGroupList(anyList());
        verify(view, atLeast(1)).printWrongInput();
    }

    @Test
    void testStartDialogWhenInputRequest2() {
        String request = "2";
        String courseName = "Test";
        when(inputReader.readInput())
                                    .thenReturn(request)
                                    .thenReturn(courseName)
                                    .thenReturn(WRONG_INPUT)
                                    .thenReturn(BACK)
                                    .thenReturn(EXIT);
        Course course = new Course("Test", "description");
        course.setCourseId(1);
        List<Course> coursesList = Collections.singletonList(course);
        when(requestHandler.getAllCourses()).thenReturn(coursesList);
        userInterface.startDialog();
        verify(view, atLeast(1)).printCoursesList(anyList());
        verify(view, atLeast(1)).printEnterCourseName();
        verify(requestHandler, atLeast(1)).getAllStudentsByCourseName(anyString());
        verify(view, atLeast(1)).printStudentsList(anyList());
        verify(view, atLeast(1)).printWrongInput();
    }

    @Test
    void testStartDialogWhenInputRequest3() {
        String request = "3";
        String groupId = "1";
        String firstName = "test";
        String lastName = "testov";
        when(inputReader.readInput())
                                    .thenReturn(request)
                                    .thenReturn(groupId)
                                    .thenReturn(firstName)
                                    .thenReturn(lastName)
                                    .thenReturn(BACK)
                                    .thenReturn(WRONG_INPUT)
                                    .thenReturn(BACK)
                                    .thenReturn(EXIT);
        Group group = new Group("TT-11");
        group.setGroupId(1);
        List<Group> groupsList = Collections.singletonList(group);
        when(requestHandler.getAllGroupsByStudents(anyInt())).thenReturn(groupsList);
        userInterface.startDialog();
        verify(view, atLeast(1)).printGroupList(groupsList);
        verify(view, atLeast(1)).printSelectGroupForStudent();
        verify(view, atLeast(1)).printEnterFirstName();
        verify(view, atLeast(1)).printEnterLastName();
        verify(requestHandler, atLeast(1)).addStudent(group, firstName, lastName);
        verify(view, atLeast(1)).printSuccessAddedStudent(anyInt());
        verify(view, atLeast(1)).printWrongInput();
    }

    @Test
    void testStartDialogWhenInputRequest4() {
        String request = "4";
        String studentId = "1";
        when(inputReader.readInput())
                                    .thenReturn(request)
                                    .thenReturn(studentId)
                                    .thenReturn(WRONG_INPUT)
                                    .thenReturn(BACK)
                                    .thenReturn(EXIT);
        Student student = new Student();
        student.setStudentId(1);
        List<Student> studentsList = Collections.singletonList(student);
        when(requestHandler.getAllStudents()).thenReturn(studentsList);
        userInterface.startDialog();
        verify(view, atLeast(1)).printStudentsList(studentsList);
        verify(view, atLeast(1)).printSelectStudentForRemove();
        verify(requestHandler, atLeast(1)).deleteStudentById(student.getStudentId());
        verify(view, atLeast(1)).printSuccessRemovedStudent(studentId);
        verify(view, atLeast(1)).printWrongInput();
    }

    @Test
    void testStartDialogWhenInputRequest5() {
        String request = "5";
        String studentId = "1";
        String courseName1 = "test1";
        String courseName2 = "test2";
        when(inputReader.readInput())
                                    .thenReturn(request)
                                    .thenReturn(WRONG_INPUT)
                                    .thenReturn(studentId)
                                    .thenReturn(courseName1)
                                    .thenReturn(courseName2)
                                    .thenReturn(BACK)
                                    .thenReturn(WRONG_INPUT)
                                    .thenReturn(BACK)
                                    .thenReturn(EXIT);
        Student student = new Student();
        student.setStudentId(1);
        Course course1 = new Course("test1", "description1");
        course1.setCourseId(1);
        Course course2 = new Course("test2", "description2");
        course2.setCourseId(2);
        List<Student> studentsList = Collections.singletonList(student);
        List<Course> coursesStudentList = Collections.singletonList(course1);
        List<Course> coursesList = Arrays.asList(course1, course2);
        when(requestHandler.getAllStudents()).thenReturn(studentsList);
        when(requestHandler.getAllCoursesForStudent(student.getStudentId())).thenReturn(coursesStudentList);
        when(requestHandler.getAllCourses()).thenReturn(coursesList);
        userInterface.startDialog();
        verify(view, atLeast(2)).printStudentsList(studentsList);
        verify(view, atLeast(2)).printSelectStudentForAddedToCourse();
        verify(view, atLeast(2)).printCoursesListForStudent(coursesStudentList, student);
        verify(view, atLeast(2)).printCoursesList(coursesList);
        verify(view, atLeast(2)).printEnterCourseForAddStudent();
        verify(view, atLeast(1)).printSuccessAddedToCourse(anyString());
        verify(view, atLeast(1)).printStudentAlreadyOnCourse();
        verify(requestHandler, atLeast(1)).addStudentToCource(anyInt(), anyInt());
        verify(view, atLeast(2)).printWrongInput();
    }

    @Test
    void testStartDialogWhenInputRequest6() {
        String request = "6";
        String studentId = "1";
        String courseName = "test1";
        when(inputReader.readInput())
                                    .thenReturn(request)
                                    .thenReturn(studentId)
                                    .thenReturn(WRONG_INPUT)
                                    .thenReturn(courseName)
                                    .thenReturn(WRONG_INPUT)
                                    .thenReturn(BACK)
                                    .thenReturn(BACK)
                                    .thenReturn(EXIT);
        Student student = new Student();
        student.setStudentId(1);
        Course course1 = new Course("test1", "description1");
        course1.setCourseId(1);
        List<Student> studentsList = Collections.singletonList(student);
        List<Course> coursesStudentList = Collections.singletonList(course1);
        when(requestHandler.getAllStudents()).thenReturn(studentsList);
        when(requestHandler.getAllCoursesForStudent(anyInt())).thenReturn(coursesStudentList);
        userInterface.startDialog();
        verify(view, atLeast(1)).printStudentsList(studentsList);
        verify(view, atLeast(1)).printEnterStudentToRemoveFromCourse();
        verify(view, atLeast(1)).printCoursesListForStudent(coursesStudentList, student);
        verify(view, atLeast(1)).printEnterCourseNameForRemoved();
        verify(requestHandler, atLeast(1)).removeStudentFromCourse(anyInt(), anyInt());
        verify(view, atLeast(1)).printSuccessRemovedFromCourse();
        verify(view, atLeast(2)).printWrongInput();
    }
}
