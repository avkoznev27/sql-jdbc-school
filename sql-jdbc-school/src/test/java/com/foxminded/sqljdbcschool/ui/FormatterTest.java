package com.foxminded.sqljdbcschool.ui;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.foxminded.sqljdbcschool.domain.model.Course;
import com.foxminded.sqljdbcschool.domain.model.Group;
import com.foxminded.sqljdbcschool.domain.model.Student;

class FormatterTest {

    private Formatter formatter;
    private Group group;
    private Course course;
    private Student student;

    @BeforeEach
    void setUp() throws Exception {
        formatter = new Formatter();
        group = new Group("TT-11");
        course = new Course("Test", "Description");
        student = new Student("Test", "Testov", group);
    }

    @Test
    void testFormatGroupListShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> formatter.formatGroupList(null));
    }

    @Test
    void testFormatGroupListShouldReturnFormattedStringWhenGroupsListPassed() {
        group.setGroupId(1);
        group.setNumberOfStudents(20);
        List<Group> groupsList = Collections.singletonList(group);

        String expected = "group_id|group_name|students\r\n" +
                          "----------------------------\n" +
                          "1       |TT-11     |20     \r\n";
        String actual = formatter.formatGroupList(groupsList);
        assertEquals(expected, actual);
    }

    @Test
    void testFormatCourseListShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> formatter.formatCourseList(null));
    }

    @Test
    void testFormatCourseListShouldReturnFormattedStringWhenCoursesListPassed() {
        course.setCourseId(1);
        List<Course> coursesList = Collections.singletonList(course);
        String expected = "course_id |course_name |course_descriptions\r\n" +
                          "-------------------------------------------\n" +
                          "1         |Test        |Description\r\n";
        String actual = formatter.formatCourseList(coursesList);
        assertEquals(expected, actual);
    }

    @Test
    void testFormatStudentListShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> formatter.formatStudentList(null));
    }

    @Test
    void testFormatStudentListShouldReturnFormattedStringWhenStudentsListPassed() {
        student.setStudentId(1);
        student.setGroupId(1);
        List<Student> studentsList = Collections.singletonList(student);
        String expected = "student_id|group_id|first_name  |last_name   \r\n" +
                          "---------------------------------------------\n" +
                          "1         |1       |Test        |Testov      \r\n";
        String actual = formatter.formatStudentList(studentsList);
        assertEquals(expected, actual);
    }
}
