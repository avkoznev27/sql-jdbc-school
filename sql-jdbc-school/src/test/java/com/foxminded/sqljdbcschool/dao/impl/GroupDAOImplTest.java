package com.foxminded.sqljdbcschool.dao.impl;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.foxminded.sqljdbcschool.dao.DataSource;
import com.foxminded.sqljdbcschool.dao.GroupDAO;
import com.foxminded.sqljdbcschool.dao.SQLScriptReader;
import com.foxminded.sqljdbcschool.domain.model.Group;
import com.foxminded.sqljdbcschool.exceptions.DAOException;
import com.foxminded.sqljdbcschool.exceptions.RuntimeDAOException;

class GroupDAOImplTest {

    private SQLScriptReader scriptReader;
    private GroupDAO groupDAO;
    private DataSource dataSourceTest;

    @BeforeEach
    void setUp() throws Exception {
        dataSourceTest = new DataSource("jdbc:h2:mem:db;DB_CLOSE_DELAY=-1");
        groupDAO = new GroupDAOImpl(dataSourceTest);
        scriptReader = new SQLScriptReader(dataSourceTest);
        scriptReader.executeScript("CreateTables.sql");
    }

    @Test
    void testGetAllShouldReturnListAllGroupsFromDataBase() throws DAOException {
        scriptReader.executeScript("testGroups.sql");
        Group testGroup1 = new Group("TT-56");
        Group testGroup2 = new Group("TT-57");
        testGroup1.setGroupId(1);
        testGroup2.setGroupId(2);
        List<Group> expected = Arrays.asList(testGroup1, testGroup2);
        List<Group> actual = groupDAO.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void testAddShouldThrowIllegalArgumentExceptionWhenGroupIsNull() {
        assertThrows(IllegalArgumentException.class, () -> groupDAO.add(null));
    }

    @Test
    void testAddShouldThrowDAOExceptionWhenGroupNameIsNull() {
        Group testGroup = new Group(null);
        assertThrows(DAOException.class, () -> groupDAO.add(testGroup));
    }

    @Test
    void testAddShouldAddGroupWhenGroupIsNotNull() throws DAOException {
        Group testGroup = new Group("DD-45");
        groupDAO.add(testGroup);
        List<Group> actual = groupDAO.getAll();
        assertEquals(singletonList(testGroup), actual);
    }

    @Test
    void testAddShouldSetGroupIdWhenGroupAddInDatatBase() throws DAOException {
        Group testGroup = new Group("DD-45");
        groupDAO.add(testGroup);
        assertNotNull(testGroup.getGroupId());
    }

    @Test
    void testAddAllShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> groupDAO.addAll(null));
    }

    @Test
    void testAddAllShouldThrowRuntimeDAOExceptionWhenGroupNameIsNull() {
        Group testGroup = new Group(null);
        assertThrows(RuntimeDAOException.class, () -> groupDAO.addAll(singletonList(testGroup)));
    }

    @Test
    void testAddAllShouldAddAllGroupsWhenGroupNameIsNotNull() throws DAOException {
        Group testGroup1 = new Group("DD-45");
        Group testGroup2 = new Group("DD-46");
        Group testGroup3 = new Group("DD-47");
        List<Group> expected = Arrays.asList(testGroup1, testGroup2, testGroup3);
        groupDAO.addAll(expected);
        assertEquals(expected, groupDAO.getAll());
    }

    @Test
    void testAddAllShouldSetAllGroupsGroupIdWhenGroupAddedToDataBase() throws DAOException {
        Group testGroup1 = new Group("DD-45");
        Group testGroup2 = new Group("DD-46");
        Group testGroup3 = new Group("DD-47");
        List<Group> groupsList = Arrays.asList(testGroup1, testGroup2, testGroup3);
        groupDAO.addAll(groupsList);
        groupsList.forEach(group -> assertNotNull(group.getGroupId()));
    }

    @Test
    void testGetGroupsByStudentShouldThrowIllegalArgumentExceptionWhenNullPassed() throws DAOException {
        assertThrows(IllegalArgumentException.class, () -> groupDAO.getGroupsByStudents(null));
    }

    @Test
    void testGetGroupsByStudentShouldReturnListGroupsIfStudentsCountEqualsOrLessThanPassed() throws DAOException {
        scriptReader.executeScript("testGroupsAndStudents.sql");
        Group testGroup1 = new Group("TT-56");
        Group testGroup2 = new Group("TT-57");
        testGroup1.setGroupId(1);
        testGroup2.setGroupId(2);
        List<Group> expected = Arrays.asList(testGroup1, testGroup2);
        List<Group> actual = groupDAO.getGroupsByStudents(3);
        assertEquals(expected, actual);
    }

    @Test
    void testGetGroupsByStudentShouldReturnEmptyListIfNoGroupsWithPassedStudentsCount() throws DAOException {
        scriptReader.executeScript("testGroupsAndStudents.sql");
        List<Group> expected = Collections.emptyList();
        List<Group> actual = groupDAO.getGroupsByStudents(1);
        assertEquals(expected, actual);
    }
}
