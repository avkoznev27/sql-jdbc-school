package com.foxminded.sqljdbcschool.dao.impl;

import static org.junit.jupiter.api.Assertions.*;
import static java.util.Collections.singletonList;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.foxminded.sqljdbcschool.dao.CourseDAO;
import com.foxminded.sqljdbcschool.dao.DataSource;
import com.foxminded.sqljdbcschool.dao.SQLScriptReader;
import com.foxminded.sqljdbcschool.domain.model.Course;
import com.foxminded.sqljdbcschool.exceptions.DAOException;
import com.foxminded.sqljdbcschool.exceptions.RuntimeDAOException;

class CourseDAOImplTest {

    private SQLScriptReader scriptReader;
    private CourseDAO courseDAO;
    private DataSource dataSourceTest;

    @BeforeEach
    void setUp() throws Exception {
        dataSourceTest = new DataSource("jdbc:h2:mem:db;DB_CLOSE_DELAY=-1");
        courseDAO = new CourseDAOImpl(dataSourceTest);
        scriptReader = new SQLScriptReader(dataSourceTest);
        scriptReader.executeScript("CreateTables.sql");
    }

    @Test
    void testGetAllShouldReturnListAllCoursesFromDataBase() throws DAOException {
        scriptReader.executeScript("testCourses.sql");
        Course testCourse1 = new Course("test1", "description1");
        Course testCourse2 = new Course("test2", "description2");
        testCourse1.setCourseId(1);
        testCourse2.setCourseId(2);
        List<Course> expected = Arrays.asList(testCourse1, testCourse2);
        List<Course> actual = courseDAO.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void testAddShouldThrowIllegalArgumentExceptionWhenCourseIsNull() {
        assertThrows(IllegalArgumentException.class, () -> courseDAO.add(null));
    }

    @Test
    void testAddShouldThrowDAOExceptionWhenCourseNameOrCourseDescriptionIsNull() {
        Course testCourse1 = new Course("test", null);
        Course testCourse2 = new Course(null, "description");
        Course testCourse3 = new Course(null, null);
        assertThrows(DAOException.class, () -> courseDAO.add(testCourse1));
        assertThrows(DAOException.class, () -> courseDAO.add(testCourse2));
        assertThrows(DAOException.class, () -> courseDAO.add(testCourse3));
    }

    @Test
    void testAddShouldAddCourseWhenCourseIsCorrect() throws DAOException {
        Course testCourse = new Course("test", "description");
        courseDAO.add(testCourse);
        List<Course> actual = courseDAO.getAll();
        assertEquals(singletonList(testCourse), actual);
    }

    @Test
    void testAddShouldSetCourseIdWhenCourseAddInDatatBase() throws DAOException {
        Course testCourse = new Course("test", "description");
        courseDAO.add(testCourse);
        assertNotNull(testCourse.getCourseId());
    }

    @Test
    void testAddAllShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> courseDAO.addAll(null));
    }

    @Test
    void testAddAllShouldThrowRuntimeDAOExceptionWhenCourseNameOrCourseDescriptionIsNull() {
        Course testCourse1 = new Course("test", null);
        Course testCourse2 = new Course(null, "description");
        Course testCourse3 = new Course(null, null);
        assertThrows(RuntimeDAOException.class, () -> courseDAO.addAll(singletonList(testCourse1)));
        assertThrows(RuntimeDAOException.class, () -> courseDAO.addAll(singletonList(testCourse2)));
        assertThrows(RuntimeDAOException.class, () -> courseDAO.addAll(singletonList(testCourse3)));
    }

    @Test
    void testAddAllShouldAddAllCoursesWhenCourseNameAndCourseDescriptionIsNotNull() throws DAOException {
        Course testCourse1 = new Course("test1", "description1");
        Course testCourse2 = new Course("test2", "description2");
        Course testCourse3 = new Course("test3", "description3");
        List<Course> expected = Arrays.asList(testCourse1, testCourse2, testCourse3);
        courseDAO.addAll(expected);
        assertEquals(expected, courseDAO.getAll());
    }

    @Test
    void testAddAllShouldSetAllCoursesCourseIdWhenCourseAddedToDataBase() throws DAOException {
        Course testCourse1 = new Course("test1", "description1");
        Course testCourse2 = new Course("test2", "description2");
        Course testCourse3 = new Course("test3", "description3");
        List<Course> courseList = Arrays.asList(testCourse1, testCourse2, testCourse3);
        courseDAO.addAll(courseList);
        courseList.forEach(course -> assertNotNull(course.getCourseId()));
    }

    @Test
    void testGetAllCoursesByStudentShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> courseDAO.getAllCoursesByStudent(null));
    }

    @Test
    void testGetAllCoursesByStudentShouldReturnEmptyListIfStudentIsNotInDataBase() throws DAOException {
        List<Course> expected = Collections.emptyList();
        List<Course> actual = courseDAO.getAllCoursesByStudent(500);
        assertEquals(expected, actual);
    }

    @Test
    void testGetAllCoursesByStudentShouldReturnListCoursesForStudentIfStudentIsInDataBase() throws DAOException {
        scriptReader.executeScript("testStudents_courses.sql");
        Course testCourse1 = new Course("test1", "description1");
        Course testCourse3 = new Course("test3", "description3");
        testCourse1.setCourseId(1);
        testCourse3.setCourseId(3);
        List<Course> expected = Arrays.asList(testCourse1, testCourse3);
        List<Course> actual = courseDAO.getAllCoursesByStudent(1);
        assertEquals(expected, actual);
    }
}
