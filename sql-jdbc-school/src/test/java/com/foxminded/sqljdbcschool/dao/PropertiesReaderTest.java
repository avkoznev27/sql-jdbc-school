package com.foxminded.sqljdbcschool.dao;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.foxminded.sqljdbcschool.exceptions.PropertiesReaderException;

class PropertiesReaderTest {

    private PropertiesReader propertiesReader;
    private static final String FILE_NAME = "test.properties";

    @BeforeEach
    void setUp() throws Exception {
        propertiesReader = new PropertiesReader(FILE_NAME);
    }

    @Test
    final void testGetPropertyValueShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> propertiesReader.getPropertyValue(null));
    }

    @Test
    final void testGetPropertyValueShouldThrowPropertiesReaderExceptionIfNotCorrectFileNameProperties() {
        propertiesReader = new PropertiesReader("nonex");
        assertThrows(PropertiesReaderException.class, () -> propertiesReader.getPropertyValue("test"));
    }

    @Test
    final void testGetPropertyValueShouldThrowPropertiesReaderExceptionWhenNotExistingPropertyNamePassed() {
        assertThrows(PropertiesReaderException.class, () -> propertiesReader.getPropertyValue("nonex"));
    }

    @Test
    final void testGetPropertyValueShouldReturnPropertyValueWhenExistingPropertyNamePassed() {
        assertEquals("test value", propertiesReader.getPropertyValue("test"));
    }
}
