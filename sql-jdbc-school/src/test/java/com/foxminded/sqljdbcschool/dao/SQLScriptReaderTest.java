package com.foxminded.sqljdbcschool.dao;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.foxminded.sqljdbcschool.exceptions.SQLScriptReaderException;

class SQLScriptReaderTest {

    private DataSource dataSource;
    private SQLScriptReader sqlScriptReader;

    @BeforeEach
    void setUp() throws Exception {
        dataSource = new DataSource("jdbc:h2:mem:db;DB_CLOSE_DELAY=-1");
        sqlScriptReader = new SQLScriptReader(dataSource);
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenScriptFileNameIsNull() {
        assertThrows(IllegalArgumentException.class, () -> sqlScriptReader.executeScript(null));
    }

    @Test
    void shouldThrowSQLScriptReaderExceptionWhenScriptFileNotExist() {
        assertThrows(SQLScriptReaderException.class, () -> sqlScriptReader.executeScript("nonex.sql"));
    }

    @Test
    void shouldThrowSQLScriptReaderExceptionWhenScriptIsWrong() {
        assertThrows(SQLScriptReaderException.class, () -> sqlScriptReader.executeScript("testWrongScript.sql"));
    }

    @Test
    void shouldExecuteScriptWhenScriptIsCorrect() {
        sqlScriptReader.executeScript("testCorrectScript.sql");
        assertDoesNotThrow(() -> sqlScriptReader.executeScript("testCorrectScript.sql"));
    }
}
