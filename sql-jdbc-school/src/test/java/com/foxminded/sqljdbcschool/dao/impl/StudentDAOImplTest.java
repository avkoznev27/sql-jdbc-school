package com.foxminded.sqljdbcschool.dao.impl;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.foxminded.sqljdbcschool.dao.CourseDAO;
import com.foxminded.sqljdbcschool.dao.DataSource;
import com.foxminded.sqljdbcschool.dao.SQLScriptReader;
import com.foxminded.sqljdbcschool.dao.StudentDAO;
import com.foxminded.sqljdbcschool.domain.model.Course;
import com.foxminded.sqljdbcschool.domain.model.Group;
import com.foxminded.sqljdbcschool.domain.model.Student;
import com.foxminded.sqljdbcschool.exceptions.DAOException;
import com.foxminded.sqljdbcschool.exceptions.RuntimeDAOException;

class StudentDAOImplTest {

    private SQLScriptReader scriptReader;
    private DataSource dataSourceTest;
    private StudentDAO studentDAO;
    private CourseDAO courseDAO;
    
    @BeforeEach
    void setUp() throws Exception {
        dataSourceTest = new DataSource("jdbc:h2:mem:db;DB_CLOSE_DELAY=-1");
        studentDAO = new StudentDAOImpl(dataSourceTest);
        courseDAO = new CourseDAOImpl(dataSourceTest);
        scriptReader = new SQLScriptReader(dataSourceTest);
        scriptReader.executeScript("CreateTables.sql");
        scriptReader.executeScript("testGroups.sql");
    }

    @Test
    void testAddShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> studentDAO.add(null));
    }

    @Test
    void testAddShouldThrowDAOExceptionWhenNameOrGroupIsNull() {
        Group testGroup = new Group("TT-56");
        testGroup.setGroupId(1);
        Student testStudent1 = new Student(null, "testov", testGroup);
        Student testStudent2 = new Student("test", null, testGroup);
        Student testStudent3 = new Student(null, null, testGroup);
        Student testStudent4 = new Student("test", "testov", null);
        assertThrows(DAOException.class, () -> studentDAO.add(testStudent1));
        assertThrows(DAOException.class, () -> studentDAO.add(testStudent2));
        assertThrows(DAOException.class, () -> studentDAO.add(testStudent3));
        assertThrows(DAOException.class, () -> studentDAO.add(testStudent4));
    }

    @Test
    void testAddShouldAddStudentWhenNameAndGroupIsNotNull() throws DAOException {
        Group testGroup = new Group("TT-56");
        testGroup.setGroupId(1);
        Student testStudent1 = new Student("test", "testov", testGroup);
        studentDAO.add(testStudent1);
        int expectedNumberOfRecords = 1;
        int actualNumberOfRecords = studentDAO.getAll().size();
        assertEquals(expectedNumberOfRecords, actualNumberOfRecords);
    }

    @Test
    void testAddShouldSetStudentIdForStudentWhenStudentAddedToDataBase() throws DAOException {
        Group testGroup = new Group("TT-56");
        testGroup.setGroupId(1);
        Student testStudent1 = new Student("test", "testov", testGroup);
        studentDAO.add(testStudent1);
        assertNotNull(testStudent1.getStudentId());
    }

    @Test
    void testGetAllShouldReturnListAllStudentsFromDataBase() throws DAOException {
        scriptReader.executeScript("testGroupsAndStudents.sql");
        int expectedNumberOfRecords = 5;
        int actualNumberOfRecords = studentDAO.getAll().size();
        assertEquals(expectedNumberOfRecords, actualNumberOfRecords);
    }

    @Test
    void testAddAllShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> studentDAO.addAll(null));
    }

    @Test
    void testAddAllShouldThrowRuntimeDAOExceptionWhenAtLeastOneStudentHasNameOrGroupOfNull() {
        Group testGroup = new Group("TT-56");
        testGroup.setGroupId(1);
        Student testStudent1 = new Student(null, "testov1", testGroup);
        Student testStudent2 = new Student("test2", "testov2", testGroup);
        Student testStudent3 = new Student("test3", "testov3", testGroup);
        Student testStudent4 = new Student("test4", "testov4", null);
        assertThrows(RuntimeDAOException.class, () -> studentDAO.addAll(Arrays.asList(testStudent1, testStudent2, testStudent3)));
        assertThrows(RuntimeDAOException.class, () -> studentDAO.addAll(Arrays.asList(testStudent2, testStudent3, testStudent4)));
    }

    @Test
    void testAddAllShouldAddListStudentsInDataBaseWhenNamesAndGroupsNotNull() throws DAOException {
        Group testGroup = new Group("TT-56");
        testGroup.setGroupId(1);
        Student testStudent1 = new Student("test1", "testov1", testGroup);
        Student testStudent2 = new Student("test2", "testov2", testGroup);
        Student testStudent3 = new Student("test3", "testov3", testGroup);
        Student testStudent4 = new Student("test4", "testov4", testGroup);
        studentDAO.addAll(Arrays.asList(testStudent1, testStudent2, testStudent3, testStudent4));
        int expectedNumberOfRecords = 4;
        int actualNumberOfRecords = studentDAO.getAll().size();
        assertEquals(expectedNumberOfRecords, actualNumberOfRecords);
    }

    @Test
    void testAddAllShouldSetStudentIdAllStudentsWhenStudentsAddedToDataBase() throws DAOException {
        Group testGroup = new Group("TT-56");
        testGroup.setGroupId(1);
        Student testStudent1 = new Student("test1", "testov1", testGroup);
        Student testStudent2 = new Student("test2", "testov2", testGroup);
        Student testStudent3 = new Student("test3", "testov3", testGroup);
        Student testStudent4 = new Student("test4", "testov4", testGroup);
        studentDAO.addAll(Arrays.asList(testStudent1, testStudent2, testStudent3, testStudent4));
        studentDAO.getAll().forEach(student -> assertNotNull(student.getStudentId()));
    }

    @Test
    void testDeleteShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> studentDAO.delete(null));
    }

    @Test
    void testDeleteShouldDeleteStudentFromDataBaseWhenStudentIdPassed() throws DAOException {
        scriptReader.executeScript("testGroupsAndStudents.sql");
        studentDAO.delete(1);
        int expectedNumberOfRecords = 4;
        int actualNumberOfRecords = studentDAO.getAll().size();
        assertEquals(expectedNumberOfRecords, actualNumberOfRecords);
    }

    @Test
    void testGetAllStudentsFromCourseShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> studentDAO.getAllStudentsFromCourse(null));
    }

    @Test
    void testGetAllStudentsFromCourseShouldReturnListAllStudentsFromCourseWhenCourseNamePassed() throws DAOException {
        scriptReader.executeScript("testStudents_courses.sql");
        int expectedNumberOfStudents = 3;
        int actualNumberOfStudents = studentDAO.getAllStudentsFromCourse("test1").size();
        assertEquals(expectedNumberOfStudents, actualNumberOfStudents);
    }

    @Test
    void testGetAllStudentsFromCourseShouldReturnEmtyListWhenCourseIsNotInDataBase() throws DAOException {
        scriptReader.executeScript("testStudents_courses.sql");
        assertEquals(Collections.emptyList(), studentDAO.getAllStudentsFromCourse("math"));
    }

    @Test
    void testAssignAllToCoursesShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> studentDAO.assignAllToCources(null));
    }

    @Test
    void testAssignAllToCoursesShouldAddAllStudentsToCoursesWhenListStudentsPassed() throws DAOException {
        scriptReader.executeScript("testStudentsAndCourses.sql");
        List<Student> studentList = new ArrayList<>();
        Group testGroup = new Group("TT-44");
        testGroup.setGroupId(1);
        Course testCourse1 = new Course("testCourse1", "description1");
        Course testCourse2 = new Course("testCourse2", "description2");
        testCourse1.setCourseId(1);
        testCourse2.setCourseId(2);
        Student testStudent1 = new Student("test1", "testov1", testGroup);
        Student testStudent2 = new Student("test2", "testov2", testGroup);
        testStudent1.setStudentId(1);
        testStudent2.setStudentId(2);
        Set<Course> coursesStud1 = new HashSet<>();
        Set<Course> coursesStud2 = new HashSet<>();
        coursesStud1.add(testCourse1);
        coursesStud2.addAll(Arrays.asList(testCourse1, testCourse2));
        testStudent1.setCourses(coursesStud1);
        testStudent2.setCourses(coursesStud2);
        studentList.addAll(Arrays.asList(testStudent1, testStudent2));
        studentDAO.assignAllToCources(studentList);
        List<Course> expectedForStudent1 = Arrays.asList(testCourse1);
        List<Course> expectedForStudent2 = Arrays.asList(testCourse1, testCourse2);
        assertEquals(expectedForStudent1, courseDAO.getAllCoursesByStudent(1));
        assertEquals(expectedForStudent2, courseDAO.getAllCoursesByStudent(2));
    }

    @Test
    void testAddToCourseShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> studentDAO.addToCourse(null, null));
        assertThrows(IllegalArgumentException.class, () -> studentDAO.addToCourse(1, null));
        assertThrows(IllegalArgumentException.class, () -> studentDAO.addToCourse(null, 1));
    }

    @Test
    void testAddToCourseShouldThrowDAOExceptionWhenStudentIdOrCourseIdDoNotExistInDataBase() {
        scriptReader.executeScript("testStudentsAndCourses.sql");
       assertThrows(DAOException.class, () -> studentDAO.addToCourse(1, 10));
       assertThrows(DAOException.class, () -> studentDAO.addToCourse(10, 1));
    }

    @Test
    void testAddToCourseShouldAddStudentToCourseWhenStudentIdAndCourseIdPassed() throws DAOException {
        scriptReader.executeScript("testStudentsAndCourses.sql");
        Course testCourse1 = new Course("testCourse1", "description1");
        Course testCourse2 = new Course("testCourse2", "description2");
        testCourse1.setCourseId(1);
        testCourse2.setCourseId(2);
        studentDAO.addToCourse(1, 1);
        studentDAO.addToCourse(1, 2);
        List<Course> expected = Arrays.asList(testCourse1, testCourse2);
        List<Course> actual = courseDAO.getAllCoursesByStudent(1);
        assertEquals(expected, actual);
    }

    @Test
    void testRemoveFromCourseShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> studentDAO.removeFromCourse(null, null));
        assertThrows(IllegalArgumentException.class, () -> studentDAO.removeFromCourse(1, null));
        assertThrows(IllegalArgumentException.class, () -> studentDAO.removeFromCourse(null, 1));
    }

    @Test
    void testRemoveFromCourseShouldRemoveStudentFromCourseWhenCourseIdAndStudentIdPassed() throws DAOException {
        scriptReader.executeScript("testStudents_courses.sql");
        Course testCourse1 = new Course("test1", "description1");
        testCourse1.setCourseId(1);
        studentDAO.removeFromCourse(1, 3);
        List<Course> expected = Arrays.asList(testCourse1);
        List<Course> actual = courseDAO.getAllCoursesByStudent(1);
        assertEquals(expected, actual);
    }
}
