package com.foxminded.sqljdbcschool;

import com.foxminded.sqljdbcschool.dao.CourseDAO;
import com.foxminded.sqljdbcschool.dao.DataSource;
import com.foxminded.sqljdbcschool.dao.GroupDAO;
import com.foxminded.sqljdbcschool.dao.SQLScriptReader;
import com.foxminded.sqljdbcschool.dao.StudentDAO;
import com.foxminded.sqljdbcschool.dao.impl.CourseDAOImpl;
import com.foxminded.sqljdbcschool.dao.impl.GroupDAOImpl;
import com.foxminded.sqljdbcschool.dao.impl.StudentDAOImpl;
import com.foxminded.sqljdbcschool.domain.RequestHandler;
import com.foxminded.sqljdbcschool.domain.TestDataLoader;
import com.foxminded.sqljdbcschool.domain.testdata.TestData;
import com.foxminded.sqljdbcschool.domain.testdata.TestDataGenerator;
import com.foxminded.sqljdbcschool.ui.InputReader;
import com.foxminded.sqljdbcschool.ui.UserInterface;
import com.foxminded.sqljdbcschool.ui.View;

public class Application {

    private static final String CREATE_TABLES_SCRIPT = "CreateTables.sql";
    private static final int STUDENTS_NUMBER = 200;
    private static final int GROUPS_NUMBER = 10;
    private static final int COURSES_NUMBER = 10;

    public static void main(String[] args) {
        DataSource dataSource = new DataSource();
        SQLScriptReader scriptReader = new SQLScriptReader(dataSource);
        scriptReader.executeScript(CREATE_TABLES_SCRIPT);
        GroupDAO groupDAO = new GroupDAOImpl(dataSource);
        CourseDAO courseDAO = new CourseDAOImpl(dataSource);
        StudentDAO studentDAO = new StudentDAOImpl(dataSource);

        TestDataGenerator generator = new TestDataGenerator();
        TestData testData = generator.generateData(STUDENTS_NUMBER, GROUPS_NUMBER, COURSES_NUMBER);
        TestDataLoader testDataLoader = new TestDataLoader();
        testDataLoader.fillTables(groupDAO, courseDAO, studentDAO, testData);

        RequestHandler requestHandler = new RequestHandler(groupDAO, courseDAO, studentDAO);
        InputReader inputReader = new InputReader();
        View view = new View();
        UserInterface ui = new UserInterface(requestHandler, inputReader, view);
        ui.startDialog();
    }
}
