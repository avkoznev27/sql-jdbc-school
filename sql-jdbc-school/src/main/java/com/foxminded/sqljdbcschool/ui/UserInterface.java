package com.foxminded.sqljdbcschool.ui;

import java.util.List;
import com.foxminded.sqljdbcschool.domain.RequestHandler;
import com.foxminded.sqljdbcschool.domain.model.Course;
import com.foxminded.sqljdbcschool.domain.model.Group;
import com.foxminded.sqljdbcschool.domain.model.Student;

public class UserInterface {

    private RequestHandler requestHandler;
    private InputReader inputReader;
    private View view;

    private static final String EXIT = "exit";
    private static final String BACK = "back";

    public UserInterface(RequestHandler requestHandler, InputReader inputReader, View view) {
        this.requestHandler = requestHandler;
        this.inputReader = inputReader;
        this.view = view;
    }

    public void startDialog() {
        view.printWelcome();
        boolean exit = false;
        while (!exit) {
            view.printAvailableRequests();
            String request = inputReader.readInput();
            switch (request.toLowerCase()) {
            case "1":
                showMenuFindGroupsByNumberOfStudents();
                break;
            case "2":
                showMenuFindAllStudentsOfCourse();
                break;
            case "3":
                showMenuAddNewStudent();
                break;
            case "4":
                showMenuDeleteStudentById();
                break;
            case "5":
                showMenuAddStudentByCourse();
                break;
            case "6":
                showMenuRemoveStudentFromCourse();
                break;
            case EXIT:
                exit = true;
                break;
            default:
                view.printWrongInput();
                break;
            }
        }
    }

    private void showMenuFindGroupsByNumberOfStudents() {
        boolean back = false;
        while (!back) {
            view.printEnterQuantityStudents();
            String inputNumberOfStudents = inputReader.readInput();
            back = findGroupsByNumberOfStudents(inputNumberOfStudents);
        }
    }

    private void showMenuFindAllStudentsOfCourse() {
        boolean back = false;
        while (!back) {
            List<Course> coursesList = requestHandler.getAllCourses();
            view.printCoursesList(coursesList);
            view.printEnterCourseName();
            String inputCourseName = inputReader.readInput();
            Course course = checkInputCourse(coursesList, inputCourseName);
            back = findAllStudentsOfCourse(course, inputCourseName);
        }
    }

    private void showMenuAddNewStudent() {
        boolean back = false;
        while (!back) {
            List<Group> groupsList = requestHandler.getAllGroupsByStudents(29);
            view.printGroupList(groupsList);
            view.printSelectGroupForStudent();
            String inputGroupId = inputReader.readInput();
            Group group = checkInputGroup(groupsList, inputGroupId);
            back = addNewStudent(group, inputGroupId);
        }
    }

    private void showMenuDeleteStudentById() {
        boolean back = false;
        while (!back) {
            List<Student> studentsList = requestHandler.getAllStudents();
            view.printStudentsList(studentsList);
            view.printSelectStudentForRemove();
            String inputStudentId = inputReader.readInput();
            Student student = checkInputStudent(studentsList, inputStudentId);
            back = deleteStudentById(student, inputStudentId);
        }
    }

    private void showMenuAddStudentByCourse() {
        boolean back = false;
        while (!back) {
            List<Student> studentsList = requestHandler.getAllStudents();
            view.printStudentsList(studentsList);
            view.printSelectStudentForAddedToCourse();
            String inputStudentId = inputReader.readInput();
            Student student = checkInputStudent(studentsList, inputStudentId);
            if (student != null) {
                addStudentByCourse(student);
            } else if (inputStudentId.equalsIgnoreCase(BACK)) {
                back = true;
            } else {
                view.printWrongInput();
            }
        }
    }

    private void showMenuRemoveStudentFromCourse() {
        boolean back = false;
        while (!back) {
            List<Student> studentsList = requestHandler.getAllStudents();
            view.printStudentsList(studentsList);
            view.printEnterStudentToRemoveFromCourse();
            String inputStudentId = inputReader.readInput();
            Student student = checkInputStudent(studentsList, inputStudentId);
            if (student != null) {
                removeStudentFromCourse(student);
            } else if (inputStudentId.equalsIgnoreCase(BACK)) {
                back = true;
            } else {
                view.printWrongInput();
            }
        }
    }

    private boolean findGroupsByNumberOfStudents(String inputNumberOfStudents) {
        if (inputNumberOfStudents.matches("\\d\\d")) {
            Integer quantity = Integer.parseInt(inputNumberOfStudents);
            List<Group> groupList = requestHandler.getAllGroupsByStudents(quantity);
            view.printGroupList(groupList);
            return false;
        } else if (inputNumberOfStudents.equalsIgnoreCase(BACK)) {
            return true;
        } else {
            view.printWrongInput();
            return false;
        }
    }

    private boolean findAllStudentsOfCourse(Course course, String inputCourseName) {
        if (course != null) {
            List<Student> studentsList = requestHandler.getAllStudentsByCourseName(course.getCourseName());
            view.printStudentsList(studentsList);
            return false;
        } else if (inputCourseName.equalsIgnoreCase(BACK)) {
            return true;
        } else {
            view.printWrongInput();
            return false;
        }
    }

    private boolean addNewStudent(Group group, String inputGroupId) {
        if (group != null) {
            view.printEnterFirstName();
            String firstName = inputReader.readInput();
            view.printEnterLastName();
            String lastName = inputReader.readInput();
            Integer newStudentId = requestHandler.addStudent(group, firstName, lastName);
            view.printSuccessAddedStudent(newStudentId);
            return false;
        } else if (inputGroupId.equalsIgnoreCase(BACK)) {
            return true;
        } else {
            view.printWrongInput();
            return false;
        }
    }

    private boolean deleteStudentById(Student student, String inputStudentId) {
        if (student != null) {
            requestHandler.deleteStudentById(student.getStudentId());
            view.printSuccessRemovedStudent(inputStudentId);
            return false;
        } else if (inputStudentId.equalsIgnoreCase(BACK)) {
            return true;
        } else {
            view.printWrongInput();
            return false;
        }
    }

    private void addStudentByCourse(Student student) {
        boolean back = false;
        while (!back) {
            List<Course> studentCourseList = requestHandler.getAllCoursesForStudent(student.getStudentId());
            view.printCoursesListForStudent(studentCourseList, student);
            List<Course> coursesList = requestHandler.getAllCourses();
            view.printCoursesList(coursesList);
            view.printEnterCourseForAddStudent();
            String inputCourseName = inputReader.readInput();
            Course course = checkInputCourse(coursesList, inputCourseName);
            if (course != null && !studentCourseList.contains(course)) {
                requestHandler.addStudentToCource(student.getStudentId(), course.getCourseId());
                view.printSuccessAddedToCourse(course.getCourseName());
            } else if (course != null && studentCourseList.contains(course)) {
                view.printStudentAlreadyOnCourse();
            } else if (inputCourseName.equalsIgnoreCase(BACK)) {
                back = true;
            } else {
                view.printWrongInput();
            }
        }
    }

    private void removeStudentFromCourse(Student student) {
        boolean back = false;
        while (!back) {
            List<Course> studentCourseList = requestHandler.getAllCoursesForStudent(student.getStudentId());
            view.printCoursesListForStudent(studentCourseList, student);
            view.printEnterCourseNameForRemoved();
            String courseName = inputReader.readInput();
            Course course = checkInputCourse(studentCourseList, courseName);
            if (course != null) {
                requestHandler.removeStudentFromCourse(student.getStudentId(), course.getCourseId());
                view.printSuccessRemovedFromCourse();
            } else if (courseName.equalsIgnoreCase(BACK)) {
                back = true;
            } else {
                view.printWrongInput();
            }
        }
    }

    private Group checkInputGroup(List<Group> groupList, String groupId) {
        return groupList.stream().filter(group -> group.getGroupId().toString().equals(groupId)).findFirst()
                .orElse(null);
    }

    private Course checkInputCourse(List<Course> coursesList, String input) {
        return coursesList.stream().filter(course -> course.getCourseName().equalsIgnoreCase(input)).findFirst()
                .orElse(null);
    }

    private Student checkInputStudent(List<Student> studentsList, String studentId) {
        return studentsList.stream().filter(student -> student.getStudentId().toString().equals(studentId)).findFirst()
                .orElse(null);
    }
}
