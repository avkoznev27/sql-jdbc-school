package com.foxminded.sqljdbcschool.ui;

import static java.util.Collections.nCopies;

import java.util.List;

import com.foxminded.sqljdbcschool.domain.model.Course;
import com.foxminded.sqljdbcschool.domain.model.Group;
import com.foxminded.sqljdbcschool.domain.model.Student;

public class Formatter {

    private static final String HORIZONTAL_BAR = "-";
    private static final String FORMAT_GROUP_TABLE = "%-8s|%-10s|%-7s%n";
    private static final String FORMAT_COURSE_TABLE = "%-10s|%-12s|%s%n";
    private static final String FORMAT_STUDENT_TABLE = "%-10s|%-8s|%-12s|%-12s%n";
    private static final String GROUP_ID = "group_id";
    private static final String GROUP_NAME = "group_name";
    private static final String STUDENTS = "students";
    private static final String COURSE_ID = "course_id";
    private static final String COURSE_NAME = "course_name";
    private static final String COURSE_DESCRIPTIONS = "course_descriptions";
    private static final String STUDENT_ID = "student_id";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String INPUT_ERROR_MESSAGE = "Input is null";

    public String formatGroupList(List<Group> groupList) {
        if (groupList == null) {
            throw new IllegalArgumentException(INPUT_ERROR_MESSAGE);
        }
        StringBuilder result = new StringBuilder();
        result.append(String.format(FORMAT_GROUP_TABLE, GROUP_ID, GROUP_NAME, STUDENTS))
              .append(drawLine(result.length() - 2));
        groupList.forEach(g -> result.append(String.format(FORMAT_GROUP_TABLE, g.getGroupId(), g.getGroupName(), g.getNumberOfStudents())));
        return result.toString();
    }

    public String formatCourseList(List<Course> courseList) {
        if (courseList == null) {
            throw new IllegalArgumentException(INPUT_ERROR_MESSAGE);
        }
        StringBuilder result = new StringBuilder();
        result.append(String.format(FORMAT_COURSE_TABLE, COURSE_ID, COURSE_NAME, COURSE_DESCRIPTIONS))
              .append(drawLine(result.length() - 2));
        courseList.forEach(c -> result.append(String.format(FORMAT_COURSE_TABLE, c.getCourseId(), c.getCourseName(), c.getCourseDescription())));
        return result.toString();
    }

    public String formatStudentList(List<Student> studentsList) {
        if (studentsList == null) {
            throw new IllegalArgumentException(INPUT_ERROR_MESSAGE);
        }
        StringBuilder result = new StringBuilder();
        result.append(String.format(FORMAT_STUDENT_TABLE, STUDENT_ID, GROUP_ID, FIRST_NAME, LAST_NAME))
              .append(drawLine(result.length() - 2));
        studentsList.forEach(s -> result.append(String.format(FORMAT_STUDENT_TABLE, s.getStudentId(), s.getGroupId(),
                s.getFirstName(), s.getLastName())));
        return result.toString();
    }

    private String drawLine(int width) {
        return String.join("", nCopies(width, HORIZONTAL_BAR)) + "\n";
    }
}
