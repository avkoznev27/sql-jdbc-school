package com.foxminded.sqljdbcschool.ui;

public class StringResources {

    private StringResources() {}

    public static final String MAIN_MENU_WELCOME = "Welcome to main menu Shcool database!\n";
    public static final String AVAILABLE_REQUESTS = "Available requests:\n"
                                                    + "1. Find all groups with less or equals student count\n"
                                                    + "2. Find all students related to course with given name\n"
                                                    + "3. Add new student\n"
                                                    + "4. Delete student by STUDENT_ID\n"
                                                    + "5. Add a student to the course (from a list)\n"
                                                    + "6. Remove the student from one of his or her courses\n"
                                                    + "Enter request number [1...6] or 'exit'";
    public static final String MAIN_MENU = "[main menu]|>";
    public static final String WRONG_INPUT = "Wrong input. Try again";

    public static final String REQUEST_1_MESSAGE = "Enter max quantity students in group, 'back' to return main menu";
    public static final String REQUEST_1 = "[request #1]|>";

    public static final String REQUEST_2_MESSAGE = "Enter course name, 'back' to return main menu";
    public static final String REQUEST_2 = "[request #2]|>";

    public static final String REQUEST_3_MESSAGE = "Select a group for new student (enter GroupId), 'back' to return main menu";
    public static final String REQUEST_3 = "[request #3]|>";
    public static final String REQUEST_3_ADD_STUDENT = "[request #3 -> add student]|>";
    public static final String FIRST_NAME = "Enter student's first name";
    public static final String LAST_NAME = "Enter student's last name";
    public static final String SUCCESFULLY_ADDED = "Student successfully added with id ";

    public static final String REQUEST_4_MESSAGE = "Select student for remove, 'back' to return main menu";
    public static final String REQUEST_4 = "[request #4]|>";

    public static final String REQUEST_5_MESSAGE = "Enter student id for added to course, 'back' to return main menu";
    public static final String REQUEST_5 = "[request #5]|>";
    public static final String REQUEST_5_ADD_COURSES = "[request #5 -> add courses]|>";

    public static final String REQUEST_6_MESSAGE = "Enter studentId to remove from course, 'back' to return main menu";
    public static final String REQUEST_6 = "[request #6]|>";
    public static final String REQUEST_6_DEL_COURSES = "[request #5 -> remove courses]|>";

    public static final String SUCCESFULLY_ADDED_TO_COURSE = "Student successfully added to course ";
    public static final String SUCCESFULLY_REMOVED = "Student successfully removed, id ";
    public static final String COURSES_LIST = "\nFull list of courses";
    public static final String COURSES_LIST_FOR_STUDENT = "\nList of courses for student ";
    public static final String ENTER_COURSE_NAME = "\nEnter the name of the course to which to add student or 'back' to return to student's choise";
    public static final String ENTER_COURSE_NAME_FOR_REMOVE = "\nEnter the name of the course from which to remove the student or 'back' to return to student's choise";
    public static final String STUDENT_ON_COURSE = "\nStudent is already on the course";
    public static final String SUCCESFULLY_REMOVED_COURSE = "The student succesfully removed from course ";
    public static final String STUDENTS_LIST = "\nStudents list";
    public static final String GROUP_LIST = "\nGroups list";
}
