package com.foxminded.sqljdbcschool.ui;

import java.util.List;

import com.foxminded.sqljdbcschool.domain.model.Course;
import com.foxminded.sqljdbcschool.domain.model.Group;
import com.foxminded.sqljdbcschool.domain.model.Student;

public class View {

    Formatter formatter;

    public View() {
        formatter = new Formatter();
    }

    public void printGroupList(List<Group> groupList) {
        System.out.println(StringResources.GROUP_LIST);
        System.out.println(formatter.formatGroupList(groupList));
    }

    public void printCoursesList(List<Course> coursesList) {
        System.out.println(StringResources.COURSES_LIST);
        System.out.println(formatter.formatCourseList(coursesList));
    }

    public void printCoursesListForStudent(List<Course> coursesList, Student student) {
        System.out.println(StringResources.COURSES_LIST_FOR_STUDENT + student.getFirstName() + " " + student.getLastName());
        System.out.println(formatter.formatCourseList(coursesList));
    }

    public void printStudentsList(List<Student> studentsList) {
        System.out.println(StringResources.STUDENTS_LIST);
        System.out.println(formatter.formatStudentList(studentsList));
    }

    public void printWrongInput() {
        System.out.println(StringResources.WRONG_INPUT);
    }

    public void printWelcome() {
        System.out.println(StringResources.MAIN_MENU_WELCOME);
    }

    public void printAvailableRequests() {
        System.out.println(StringResources.AVAILABLE_REQUESTS);
        System.out.print(StringResources.MAIN_MENU);
    }

    public void printEnterQuantityStudents() {
        System.out.println(StringResources.REQUEST_1_MESSAGE);
        System.out.print(StringResources.REQUEST_1);
    }

    public void printEnterCourseName() {
        System.out.println(StringResources.REQUEST_2_MESSAGE);
        System.out.print(StringResources.REQUEST_2);
    }

    public void printSelectGroupForStudent() {
        System.out.println(StringResources.REQUEST_3_MESSAGE);
        System.out.print(StringResources.REQUEST_3);
    }

    public void printSelectStudentForRemove() {
        System.out.println(StringResources.REQUEST_4_MESSAGE);
        System.out.print(StringResources.REQUEST_4);
    }

    public void printSelectStudentForAddedToCourse() {
        System.out.println(StringResources.REQUEST_5_MESSAGE);
        System.out.print(StringResources.REQUEST_5);
    }

    public void printEnterStudentToRemoveFromCourse() {
        System.out.println(StringResources.REQUEST_6_MESSAGE);
        System.out.print(StringResources.REQUEST_6);
    }

    public void printEnterFirstName() {
        System.out.println(StringResources.FIRST_NAME);
        System.out.print(StringResources.REQUEST_3_ADD_STUDENT);
    }

    public void printEnterLastName() {
        System.out.println(StringResources.LAST_NAME);
        System.out.print(StringResources.REQUEST_3_ADD_STUDENT);
    }

    public void printSuccessAddedStudent(Integer newStudentId) {
        System.out.println(StringResources.SUCCESFULLY_ADDED + newStudentId);
    }

    public void printSuccessRemovedStudent(String inputStudentId) {
        System.out.println(StringResources.SUCCESFULLY_REMOVED + inputStudentId);
    }

    public void printEnterCourseForAddStudent() {
        System.out.println(StringResources.ENTER_COURSE_NAME);
        System.out.print(StringResources.REQUEST_5_ADD_COURSES);
    }

    public void printSuccessAddedToCourse(String courseName) {
        System.out.println(StringResources.SUCCESFULLY_ADDED_TO_COURSE + courseName);
    }

    public void printStudentAlreadyOnCourse() {
        System.out.println(StringResources.STUDENT_ON_COURSE);
    }

    public void printEnterCourseNameForRemoved() {
        System.out.println(StringResources.ENTER_COURSE_NAME_FOR_REMOVE);
        System.out.print(StringResources.REQUEST_6_DEL_COURSES);
    }

    public void printSuccessRemovedFromCourse() {
        System.out.println(StringResources.SUCCESFULLY_REMOVED_COURSE);
    }
}
