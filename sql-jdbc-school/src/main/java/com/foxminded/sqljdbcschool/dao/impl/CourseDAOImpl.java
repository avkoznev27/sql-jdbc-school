package com.foxminded.sqljdbcschool.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.foxminded.sqljdbcschool.dao.CourseDAO;
import com.foxminded.sqljdbcschool.dao.DataSource;
import com.foxminded.sqljdbcschool.dao.PropertiesReader;
import com.foxminded.sqljdbcschool.domain.model.Course;
import com.foxminded.sqljdbcschool.exceptions.DAOException;
import com.foxminded.sqljdbcschool.exceptions.RuntimeDAOException;

public class CourseDAOImpl implements CourseDAO {

    private DataSource dataSource;
    private PropertiesReader propertiesReader;

    private static final String QUERIES_PROPERTIES_FILE = "queries.properties";
    private static final String GET_ALL_COURSES = "GET_ALL_COURSES";
    private static final String ADD_COURSE = "ADD_COURSE";
    private static final String GET_ALL_COURSES_BY_STUDENT_ID = "GET_ALL_COURSES_BY_STUDENT_ID"; 
    
    private static final String INPUT_ERROR_MESSAGE = "Input is null";

    public CourseDAOImpl(DataSource dataSource) {
        this.dataSource = dataSource;
        propertiesReader = new PropertiesReader(QUERIES_PROPERTIES_FILE);
    }

    @Override
    public void add(Course course) throws DAOException {
        if (course == null) {
            throw new IllegalArgumentException(INPUT_ERROR_MESSAGE);
        }
        try (Connection con = dataSource.getConnection();
                PreparedStatement statement = con.prepareStatement(propertiesReader.getPropertyValue(ADD_COURSE),
                        Statement.RETURN_GENERATED_KEYS);) {
            statement.setString(1, course.getCourseName());
            statement.setString(2, course.getCourseDescription());
            statement.executeUpdate();
            setGeneratedId(statement.getGeneratedKeys(), course);
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public List<Course> getAll() throws DAOException {
        try (Connection con = dataSource.getConnection();
                PreparedStatement statement = con.prepareStatement(propertiesReader.getPropertyValue(GET_ALL_COURSES));
                ResultSet resultSet = statement.executeQuery();) {
            List<Course> coursesList = new ArrayList<>();
            while (resultSet.next()) {
                Course course = new Course();
                course.setCourseId(resultSet.getInt(1));
                course.setCourseName(resultSet.getString(2));
                course.setCourseDescription(resultSet.getString(3));
                coursesList.add(course);
            }
            return coursesList;
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public void addAll(List<Course> list) throws DAOException {
        if (list == null) {
            throw new IllegalArgumentException(INPUT_ERROR_MESSAGE);
        }
        try (Connection con = dataSource.getConnection();
                PreparedStatement statement = con.prepareStatement(propertiesReader.getPropertyValue(ADD_COURSE),
                        Statement.RETURN_GENERATED_KEYS);) {
            list.forEach(course -> {
                try {
                    statement.setString(1, course.getCourseName());
                    statement.setString(2, course.getCourseDescription());
                    statement.executeUpdate();
                    setGeneratedId(statement.getGeneratedKeys(), course);
                } catch (SQLException e) {
                    throw new RuntimeDAOException(e.getMessage(), e);
                }
            });
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public List<Course> getAllCoursesByStudent(Integer studentId) throws DAOException {
        if (studentId == null) {
            throw new IllegalArgumentException(INPUT_ERROR_MESSAGE);
        }
        try (Connection con = dataSource.getConnection();
                PreparedStatement statement = con.prepareStatement(propertiesReader.getPropertyValue(GET_ALL_COURSES_BY_STUDENT_ID))) {
            statement.setInt(1, studentId);
            List<Course> coursesList = new ArrayList<>();
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Course course = new Course();
                    course.setCourseId(resultSet.getInt(1));
                    course.setCourseName(resultSet.getString(2));
                    course.setCourseDescription(resultSet.getString(3));
                    coursesList.add(course);
                }
                return coursesList;
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    private void setGeneratedId(ResultSet resultSet, Course course) throws SQLException {
        try (ResultSet generatedKeys = resultSet) {
            if (generatedKeys.next()) {
                course.setCourseId(generatedKeys.getInt(1));
            } else {
                throw new SQLException("Creating course failed, no Id obtained.");
            }
        }
    }
}
