package com.foxminded.sqljdbcschool.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataSource {

    private String url;
    private String user;
    private String password;

    public DataSource(String url) {
        this.url = url;
    }

    public DataSource() {
        this.url = "jdbc:postgresql://localhost:5432/school";
        this.user = "userdb";
        this.password = "user";
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }
}
