package com.foxminded.sqljdbcschool.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.foxminded.sqljdbcschool.exceptions.PropertiesReaderException;

public class PropertiesReader {

    private String propertiesFileName;

    public PropertiesReader(String propertiesFileName) {
        this.propertiesFileName = propertiesFileName;
    }

    public String getPropertyValue(String propertyName) {
        if (propertyName == null) {
            throw new IllegalArgumentException("Property name is null");
        }
        Properties properties = new Properties();
        try (InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(propertiesFileName)) {
            if (inputStream == null) {
                throw new PropertiesReaderException("File name is not correct or file doesn't exist");
            }
            properties.load(inputStream);
            String propertyValue = properties.getProperty(propertyName);
            if (propertyValue == null) {
                throw new PropertiesReaderException("Property name is not correct or property doesn't exist");
            }
            return propertyValue;
        } catch (IOException e) {
            throw new PropertiesReaderException(e.getMessage(), e);
        }
    }
}
