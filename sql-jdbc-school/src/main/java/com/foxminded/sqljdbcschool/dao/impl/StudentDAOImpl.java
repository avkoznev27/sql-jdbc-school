package com.foxminded.sqljdbcschool.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.foxminded.sqljdbcschool.dao.DataSource;
import com.foxminded.sqljdbcschool.dao.PropertiesReader;
import com.foxminded.sqljdbcschool.dao.StudentDAO;
import com.foxminded.sqljdbcschool.domain.model.Student;
import com.foxminded.sqljdbcschool.exceptions.DAOException;
import com.foxminded.sqljdbcschool.exceptions.RuntimeDAOException;

public class StudentDAOImpl implements StudentDAO {

    private DataSource dataSource;
    private PropertiesReader propertiesReader;

    private static final String QUERIES_PROPERTIES_FILE = "queries.properties";
    private static final String ADD_STUDENT = "ADD_STUDENT";
    private static final String ADD_STUDENT_TO_COURSE = "ADD_STUDENT_TO_COURSE";
    private static final String GET_ALL_STUDENTS_FROM_COURSE = "GET_ALL_STUDENTS_FROM_COURSE";
    private static final String DELETE_STUDENT = "DELETE_STUDENT";
    private static final String DELETE_STUDENT_FROM_COURSE = "DELETE_STUDENT_FROM_COURSE";
    private static final String GET_ALL_STUDENTS = "GET_ALL_STUDENTS";
    private static final String INPUT_ERROR_MESSAGE = "Input is null";
    private static final String STUDENT_ID_NULL = "StudentId is null";
    private static final String COURSE_ID_NULL = "CourseId is null";

    public StudentDAOImpl(DataSource dataSource) {
        this.dataSource = dataSource;
        propertiesReader = new PropertiesReader(QUERIES_PROPERTIES_FILE);
    }

    @Override
    public void add(Student student) throws DAOException {
        if (student == null) {
            throw new IllegalArgumentException(INPUT_ERROR_MESSAGE);
        }
        try (Connection con = dataSource.getConnection();
                PreparedStatement statement = con.prepareStatement(propertiesReader.getPropertyValue(ADD_STUDENT),
                        Statement.RETURN_GENERATED_KEYS);) {
            if (student.getGroup() == null) {
                throw new DAOException("This student has no group");
            }
            statement.setInt(1, student.getGroup().getGroupId());
            statement.setString(2, student.getFirstName());
            statement.setString(3, student.getLastName());
            statement.executeUpdate();
            setGeneratedId(statement.getGeneratedKeys(), student);
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public List<Student> getAll() throws DAOException {
        try (Connection con = dataSource.getConnection();
                PreparedStatement statement = con.prepareStatement(propertiesReader.getPropertyValue(GET_ALL_STUDENTS));
                ResultSet resultSet = statement.executeQuery();) {
            List<Student> studentList = new ArrayList<>();
            while (resultSet.next()) {
                Student student = new Student();
                student.setStudentId(resultSet.getInt(1));
                student.setGroupId(resultSet.getInt(2));
                student.setFirstName(resultSet.getString(3));
                student.setLastName(resultSet.getString(4));
                studentList.add(student);
            }
            return studentList;
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public void addAll(List<Student> list) throws DAOException {
        if (list == null) {
            throw new IllegalArgumentException(INPUT_ERROR_MESSAGE);
        }
        try (Connection con = dataSource.getConnection();
                PreparedStatement statement = con.prepareStatement(propertiesReader.getPropertyValue(ADD_STUDENT),
                        Statement.RETURN_GENERATED_KEYS);) {
            list.forEach(student -> {
                try {
                    if (student.getGroup() == null) {
                        throw new RuntimeDAOException("This student has no group: " + student.toString());
                    }
                    statement.setInt(1, student.getGroup().getGroupId());
                    statement.setString(2, student.getFirstName());
                    statement.setString(3, student.getLastName());
                    statement.executeUpdate();
                    setGeneratedId(statement.getGeneratedKeys(), student);
                } catch (SQLException e) {
                    throw new RuntimeDAOException(e.getMessage(), e);
                }
            });
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public void delete(Integer id) throws DAOException {
        if (id == null) {
            throw new IllegalArgumentException(INPUT_ERROR_MESSAGE);
        }
        try (Connection con = dataSource.getConnection();
                PreparedStatement statement = con.prepareStatement(propertiesReader.getPropertyValue(DELETE_STUDENT))) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public List<Student> getAllStudentsFromCourse(String courseName) throws DAOException {
        if (courseName == null) {
            throw new IllegalArgumentException(INPUT_ERROR_MESSAGE);
        }
        try (Connection con = dataSource.getConnection();
                PreparedStatement statement = con.prepareStatement(propertiesReader.getPropertyValue(GET_ALL_STUDENTS_FROM_COURSE));) {
            statement.setString(1, courseName);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Student> studentList = new ArrayList<>();
                while (resultSet.next()) {
                    Student student = new Student();
                    student.setStudentId(resultSet.getInt(1));
                    student.setGroupId(resultSet.getInt(2));
                    student.setFirstName(resultSet.getString(3));
                    student.setLastName(resultSet.getString(4));
                    studentList.add(student);
                }
                return studentList;
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public void assignAllToCources(List<Student> studentList) throws DAOException {
        if (studentList == null) {
            throw new IllegalArgumentException(INPUT_ERROR_MESSAGE);
        }
        try (Connection con = dataSource.getConnection();
                PreparedStatement statement = con.prepareStatement(propertiesReader.getPropertyValue(ADD_STUDENT_TO_COURSE));) {
            studentList.forEach(st -> st.getCourses().forEach(course -> {
                try {
                    statement.setInt(1, st.getStudentId());
                    statement.setInt(2, course.getCourseId());
                    statement.addBatch();
                } catch (SQLException e) {
                    throw new RuntimeDAOException(e.getMessage(), e);
                }
            }));
            statement.executeBatch();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public void addToCourse(Integer studentId, Integer courseId) throws DAOException {
        if (studentId == null) {
            throw new IllegalArgumentException(STUDENT_ID_NULL);
        }
        if (courseId == null) {
            throw new IllegalArgumentException(COURSE_ID_NULL);
        }
        try (Connection con = dataSource.getConnection();
                PreparedStatement statement = con.prepareStatement(propertiesReader.getPropertyValue(ADD_STUDENT_TO_COURSE));) {
            statement.setInt(1, studentId);
            statement.setInt(2, courseId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public void removeFromCourse(Integer studentId, Integer courseId) throws DAOException {
        if (studentId == null) {
            throw new IllegalArgumentException(STUDENT_ID_NULL);
        }
        if (courseId == null) {
            throw new IllegalArgumentException(COURSE_ID_NULL);
        }
        try (Connection con = dataSource.getConnection();
                PreparedStatement statement = con.prepareStatement(propertiesReader.getPropertyValue(DELETE_STUDENT_FROM_COURSE));) {
            statement.setInt(1, studentId);
            statement.setInt(2, courseId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    private void setGeneratedId(ResultSet resultSet, Student student) throws SQLException {
        try (ResultSet generatedKeys = resultSet) {
            if (generatedKeys.next()) {
                student.setStudentId(generatedKeys.getInt(1));
            } else {
                throw new SQLException("Creating student failed, no id obtained." + student);
            }
        }
    }
}
