package com.foxminded.sqljdbcschool.dao;

import java.util.List;

import com.foxminded.sqljdbcschool.domain.model.Student;
import com.foxminded.sqljdbcschool.exceptions.DAOException;

public interface StudentDAO {

    void add(Student student) throws DAOException;

    void addAll(List<Student> list) throws DAOException;

    void delete(Integer id) throws DAOException;

    void assignAllToCources(List<Student> studentList) throws DAOException;

    void addToCourse(Integer studentId, Integer courseId) throws DAOException;

    void removeFromCourse(Integer studentId, Integer courseId) throws DAOException;

    List<Student> getAll() throws DAOException;

    List<Student> getAllStudentsFromCourse(String courseName) throws DAOException;
}
