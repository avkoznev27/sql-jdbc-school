package com.foxminded.sqljdbcschool.dao;

import java.io.IOException;
import java.io.Reader;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.jdbc.RuntimeSqlException;
import org.apache.ibatis.jdbc.ScriptRunner;

import com.foxminded.sqljdbcschool.exceptions.SQLScriptReaderException;

public class SQLScriptReader {

    private DataSource dataSource;

    public SQLScriptReader(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void executeScript(String fileName) {
        if (fileName == null) {
            throw new IllegalArgumentException("File name is null");
        }
        try (Connection con = dataSource.getConnection();
                Reader reader = Resources.getResourceAsReader(fileName);) {
            ScriptRunner runner = new ScriptRunner(con);
            runner.runScript(reader);
        } catch (RuntimeSqlException | IOException | SQLException e) {
            throw new SQLScriptReaderException(e.getMessage(), e);
        }
    }
}
