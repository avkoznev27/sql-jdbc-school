package com.foxminded.sqljdbcschool.dao;

import java.util.List;

import com.foxminded.sqljdbcschool.domain.model.Group;
import com.foxminded.sqljdbcschool.exceptions.DAOException;

public interface GroupDAO {

    void add(Group group) throws DAOException;

    void addAll(List<Group> list) throws DAOException;

    List<Group> getAll() throws DAOException;

    List<Group> getGroupsByStudents(Integer quantity) throws DAOException;
}
