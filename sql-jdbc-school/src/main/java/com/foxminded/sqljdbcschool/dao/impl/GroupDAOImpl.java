package com.foxminded.sqljdbcschool.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.foxminded.sqljdbcschool.dao.DataSource;
import com.foxminded.sqljdbcschool.dao.GroupDAO;
import com.foxminded.sqljdbcschool.dao.PropertiesReader;
import com.foxminded.sqljdbcschool.domain.model.Group;
import com.foxminded.sqljdbcschool.exceptions.DAOException;
import com.foxminded.sqljdbcschool.exceptions.RuntimeDAOException;

public class GroupDAOImpl implements GroupDAO {

    private DataSource dataSource;
    private PropertiesReader propertiesReader;

    private static final String QUERIES_PROPERTIES_FILE = "queries.properties";
    private static final String GET_ALL_GROUPS = "GET_ALL_GROUPS";
    private static final String ADD_GROUP = "ADD_GROUP";
    private static final String GET_GROUPS_NO_MORE_THAN = "GET_GROUPS_NO_MORE_THAN";
    private static final String INPUT_ERROR_MESSAGE = "Input is null";

    public GroupDAOImpl(DataSource dataSource) {
        this.dataSource = dataSource;
        propertiesReader = new PropertiesReader(QUERIES_PROPERTIES_FILE);
    }

    @Override
    public void add(Group group) throws DAOException {
        if (group == null) {
            throw new IllegalArgumentException(INPUT_ERROR_MESSAGE);
        }
        try (Connection con = dataSource.getConnection();
                PreparedStatement statement = con.prepareStatement(propertiesReader.getPropertyValue(ADD_GROUP),
                        Statement.RETURN_GENERATED_KEYS);) {
            statement.setString(1, group.getGroupName());
            statement.executeUpdate();
            setGeneratedId(statement.getGeneratedKeys(), group);
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public void addAll(List<Group> list) throws DAOException {
        if (list == null) {
            throw new IllegalArgumentException(INPUT_ERROR_MESSAGE);
        }
        try (Connection con = dataSource.getConnection();
                PreparedStatement statement = con.prepareStatement(propertiesReader.getPropertyValue(ADD_GROUP),
                        Statement.RETURN_GENERATED_KEYS);) {
            list.forEach(group -> {
                try {
                    statement.setString(1, group.getGroupName());
                    statement.executeUpdate();
                    setGeneratedId(statement.getGeneratedKeys(), group);
                } catch (SQLException e) {
                    throw new RuntimeDAOException(e.getMessage(), e);
                }
            });
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public List<Group> getAll() throws DAOException {
        try (Connection con = dataSource.getConnection();
                PreparedStatement statement = con.prepareStatement(propertiesReader.getPropertyValue(GET_ALL_GROUPS));
                ResultSet resultSet = statement.executeQuery();) {
            List<Group> groupList = new ArrayList<>();
            while (resultSet.next()) {
                Group group = new Group();
                group.setGroupId(resultSet.getInt(1));
                group.setGroupName(resultSet.getString(2));
                groupList.add(group);
            }
            return groupList;
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    @Override
    public List<Group> getGroupsByStudents(Integer quantity) throws DAOException {
        if (quantity == null) {
            throw new IllegalArgumentException(INPUT_ERROR_MESSAGE);
        }
        try (Connection con = dataSource.getConnection();
                PreparedStatement statement = con
                        .prepareStatement(propertiesReader.getPropertyValue(GET_GROUPS_NO_MORE_THAN));) {
            statement.setInt(1, quantity);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Group> groupList = new ArrayList<>();
                while (resultSet.next()) {
                    Group group = new Group();
                    group.setGroupId(resultSet.getInt(1));
                    group.setGroupName(resultSet.getString(2));
                    group.setNumberOfStudents(resultSet.getInt(3));
                    groupList.add(group);
                }
                return groupList;
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    private void setGeneratedId(ResultSet resultSet, Group group) throws SQLException {
        try (ResultSet generatedKeys = resultSet) {
            if (generatedKeys.next()) {
                group.setGroupId(generatedKeys.getInt(1));
            } else {
                throw new SQLException("Creating group failed, no id obtained.");
            }
        }
    }
}
