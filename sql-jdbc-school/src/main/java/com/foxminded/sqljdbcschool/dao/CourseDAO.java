package com.foxminded.sqljdbcschool.dao;

import java.util.List;

import com.foxminded.sqljdbcschool.domain.model.Course;
import com.foxminded.sqljdbcschool.exceptions.DAOException;

public interface CourseDAO {

    void add(Course course) throws DAOException;

    void addAll(List<Course> list) throws DAOException;

    List<Course> getAll() throws DAOException;

    List<Course> getAllCoursesByStudent(Integer studentId) throws DAOException;
}
