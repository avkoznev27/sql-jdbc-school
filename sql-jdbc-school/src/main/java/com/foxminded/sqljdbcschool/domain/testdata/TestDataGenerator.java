package com.foxminded.sqljdbcschool.domain.testdata;

import java.util.List;

import com.foxminded.sqljdbcschool.domain.model.Course;
import com.foxminded.sqljdbcschool.domain.model.Group;
import com.foxminded.sqljdbcschool.domain.model.Student;

public class TestDataGenerator {

    private Generator<Group> groupsGenerator;
    private Generator<Student> studentsGenerator;
    private Generator<Course> coursesGenerator;

    public TestDataGenerator() {
        this.groupsGenerator = new GroupGenerator();
        this.studentsGenerator = new StudentGenerator();
        this.coursesGenerator = new CourseGenerator();
    }

    public TestData generateData(int studentsQuantity, int groupsQuantity, int coursesQuantity) {
        TestData testData = new TestData();
        List<Group> groups = groupsGenerator.generate(groupsQuantity);
        List<Student> students = studentsGenerator.generate(studentsQuantity);
        List<Course> courses = coursesGenerator.generate(coursesQuantity);
        Distributor groupDistributor = new GroupDistributor(students, groups);
        Distributor courseDistributor = new CourseDistributor(students, courses);
        groupDistributor.distribute();
        courseDistributor.distribute();
        testData.setGroups(groups);
        testData.setStudents(students);
        testData.setCourses(courses);
        return testData;
    }
}
