package com.foxminded.sqljdbcschool.domain.testdata;

import java.util.List;

public interface Generator<E> {

    List<E> generate(Integer quantity);
}
