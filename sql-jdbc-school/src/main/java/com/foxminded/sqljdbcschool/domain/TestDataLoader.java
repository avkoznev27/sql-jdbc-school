package com.foxminded.sqljdbcschool.domain;

import com.foxminded.sqljdbcschool.dao.CourseDAO;
import com.foxminded.sqljdbcschool.dao.GroupDAO;
import com.foxminded.sqljdbcschool.dao.StudentDAO;
import com.foxminded.sqljdbcschool.domain.testdata.TestData;
import com.foxminded.sqljdbcschool.exceptions.DAOException;
import com.foxminded.sqljdbcschool.exceptions.TestDataLoaderException;

public class TestDataLoader {

    public void fillTables(GroupDAO groupDAO, CourseDAO courseDAO, StudentDAO studentDAO, TestData testData) {
        checkNull(groupDAO, courseDAO, studentDAO, testData);
        try {
            groupDAO.addAll(testData.getGroups());
            studentDAO.addAll(testData.getStudents());
            courseDAO.addAll(testData.getCourses());
            studentDAO.assignAllToCources(testData.getStudents());
        } catch (DAOException e) {
            throw new TestDataLoaderException(e.getMessage(), e);
        }
    }

    private void checkNull(GroupDAO groupDAO, CourseDAO courseDAO, StudentDAO studentDAO, TestData testData) {
        if (groupDAO == null) {
            throw new IllegalArgumentException("GroupDAO is null");
        }
        if (courseDAO == null) {
            throw new IllegalArgumentException("CourseDAO is null");
        }
        if (studentDAO == null) {
            throw new IllegalArgumentException("StudentDAO is null");
        }
        if (testData == null) {
            throw new IllegalArgumentException("TestData is null");
        }
    }
}
