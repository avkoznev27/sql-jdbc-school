package com.foxminded.sqljdbcschool.domain.testdata;

import java.util.List;

import com.foxminded.sqljdbcschool.domain.model.Course;
import com.foxminded.sqljdbcschool.domain.model.Group;
import com.foxminded.sqljdbcschool.domain.model.Student;

public class TestData {
    
    private List<Group> groups;
    private List<Student> students;
    private List<Course> courses;
    
    public List<Group> getGroups() {
        return groups;
    }
    public List<Student> getStudents() {
        return students;
    }
    public List<Course> getCourses() {
        return courses;
    }
    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }
    public void setStudents(List<Student> students) {
        this.students = students;
    }
    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }
}
