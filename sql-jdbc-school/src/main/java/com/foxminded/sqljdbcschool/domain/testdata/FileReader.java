package com.foxminded.sqljdbcschool.domain.testdata;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import com.foxminded.sqljdbcschool.exceptions.FileReaderException;

public class FileReader {

    public List<String> read(String fileName) {
        if (fileName == null) {
            throw new FileReaderException("File name is null");
        }
        Path path;
        List<String> allLines;
        URL fileURL = this.getClass().getClassLoader().getResource(fileName);
        if (fileURL == null) {
            throw new FileReaderException("File name is not correct " + fileURL);
        }
        try {
            path = Paths.get(fileURL.toURI());
            allLines = Files.readAllLines(path);
        } catch (URISyntaxException | IOException e) {
            throw new FileReaderException(e.getMessage(), e);
        }
        if (allLines.isEmpty()) {
            throw new FileReaderException("File is empty");
        }
        return allLines;
    }
}
