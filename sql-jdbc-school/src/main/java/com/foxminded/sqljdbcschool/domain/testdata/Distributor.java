package com.foxminded.sqljdbcschool.domain.testdata;

public interface Distributor {

    void distribute();

}
