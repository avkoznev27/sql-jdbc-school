package com.foxminded.sqljdbcschool.domain.testdata;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.RandomStringUtils;

import com.foxminded.sqljdbcschool.domain.model.Group;

public class GroupGenerator implements Generator<Group>{

    private Random random;
    private static final String HYPHEN = "-";

    public GroupGenerator() {
        random = new Random();
    }

    @Override
    public List<Group> generate(Integer quantity) {
        if (quantity == null) {
            throw new IllegalArgumentException("Quantity of groups is null");
        }
        return Stream.iterate(0, n -> n + 1)
                .limit(quantity)
                .map(n -> {
                    String prefix = RandomStringUtils.randomAlphabetic(2).toUpperCase();
                    int postfix = 10 + random.nextInt(90);
                    Group group = new Group();
                    group.setGroupName(prefix + HYPHEN + postfix);
                    return group;
                })
                .collect(Collectors.toList());
    }
}
