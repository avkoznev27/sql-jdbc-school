package com.foxminded.sqljdbcschool.domain.testdata;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.foxminded.sqljdbcschool.domain.model.Course;

public class CourseGenerator implements Generator<Course> {

    private FileReader fileReader;

    public CourseGenerator() {
        fileReader = new FileReader();
    }

    @Override
    public List<Course> generate(Integer quantity) {
        if (quantity == null) {
            throw new IllegalArgumentException("Quantity of courses is null");
        }
        List<String> coursesList = fileReader.read("courses.txt");
        Collections.shuffle(coursesList);
        return Stream.iterate(0, n -> n + 1)
                .limit(quantity)
                .map(n -> {
                    Course course = new Course();
                    String courseName = coursesList.get(n);
                    course.setCourseName(courseName);
                    course.setCourseDescription("Description of the course in " + courseName);
                    return course;
                })
                .collect(Collectors.toList());
    }
}
