package com.foxminded.sqljdbcschool.domain.testdata;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.foxminded.sqljdbcschool.domain.model.Student;

public class StudentGenerator implements Generator<Student> {

    private Random random;
    private FileReader fileReader;

    public StudentGenerator() {
        random = new Random();
        fileReader = new FileReader();
    }

    @Override
    public List<Student> generate(Integer quantity) {
        if (quantity == null) {
            throw new IllegalArgumentException("Quantity of students is null");
        }
        List<String> listFirstNames = fileReader.read("firstNames.txt");
        List<String> listLastNames = fileReader.read("lastNames.txt");
        return Stream.iterate(0, n -> n + 1)
                .limit(quantity)
                .map(x -> {
                    Student student = new Student();
                    student.setFirstName(listFirstNames.get(random.nextInt(listFirstNames.size())));
                    student.setLastName(listLastNames.get(random.nextInt(listLastNames.size())));
                    return student;
                })
                .collect(Collectors.toList());
    }
}
