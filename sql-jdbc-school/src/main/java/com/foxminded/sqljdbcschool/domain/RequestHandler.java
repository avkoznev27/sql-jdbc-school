package com.foxminded.sqljdbcschool.domain;

import java.util.List;

import com.foxminded.sqljdbcschool.dao.CourseDAO;
import com.foxminded.sqljdbcschool.dao.GroupDAO;
import com.foxminded.sqljdbcschool.dao.StudentDAO;
import com.foxminded.sqljdbcschool.domain.model.Course;
import com.foxminded.sqljdbcschool.domain.model.Group;
import com.foxminded.sqljdbcschool.domain.model.Student;
import com.foxminded.sqljdbcschool.exceptions.DAOException;
import com.foxminded.sqljdbcschool.exceptions.RequestHandlerException;

public class RequestHandler {

    private GroupDAO groupDAO;
    private CourseDAO courseDAO;
    private StudentDAO studentDAO;

    public RequestHandler(GroupDAO groupDAO, CourseDAO courseDAO, StudentDAO studentDAO) {
        this.groupDAO = groupDAO;
        this.courseDAO = courseDAO;
        this.studentDAO = studentDAO;
    }

    public Integer addStudent(Group group, String lastName, String firstName) {
        try {
            Student student = new Student();
            student.setGroup(group);
            student.setFirstName(firstName);
            student.setLastName(lastName);
            studentDAO.add(student);
            return student.getStudentId();
        } catch (DAOException e) {
            throw new RequestHandlerException(e.getMessage(), e);
        }
    }

    public void addStudentToCource(Integer studentId, Integer courseId) {
        try {
            studentDAO.addToCourse(studentId, courseId);
        } catch (DAOException e) {
            throw new RequestHandlerException(e.getMessage(), e);
        }
    }

    public List<Student> getAllStudents() {
        try {
            return studentDAO.getAll();
        } catch (DAOException e) {
            throw new RequestHandlerException(e.getMessage(), e);
        }
    }

    public void deleteStudentById(Integer id) {
        try {
            studentDAO.delete(id);
        } catch (DAOException e) {
            throw new RequestHandlerException(e.getMessage(), e);
        }
    }

    public List<Student> getAllStudentsByCourseName(String courseName) {
        try {
            return studentDAO.getAllStudentsFromCourse(courseName);
        } catch (DAOException e) {
            throw new RequestHandlerException(e.getMessage(), e);
        }
    }

    public List<Group> getAllGroups() {
        try {
            return groupDAO.getAll();
        } catch (DAOException e) {
            throw new RequestHandlerException(e.getMessage(), e);
        }
    }

    public List<Group> getAllGroupsByStudents(Integer quantity) {
        try {
            return groupDAO.getGroupsByStudents(quantity);
        } catch (DAOException e) {
            throw new RequestHandlerException(e.getMessage(), e);
        }
    }

    public List<Course> getAllCourses() {
        try {
            return courseDAO.getAll();
        } catch (DAOException e) {
            throw new RequestHandlerException(e.getMessage(), e);
        }
    }

    public List<Course> getAllCoursesForStudent(Integer studentId) {
        try {
            return courseDAO.getAllCoursesByStudent(studentId);
        } catch (DAOException e) {
            throw new RequestHandlerException(e.getMessage(), e);
        }
    }

    public void removeStudentFromCourse(Integer studentId, Integer courseId) {
        try {
            studentDAO.removeFromCourse(studentId, courseId);
        } catch (DAOException e) {
            throw new RequestHandlerException(e.getMessage(), e);
        }
    }
}
