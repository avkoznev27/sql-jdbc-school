package com.foxminded.sqljdbcschool.domain.testdata;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.foxminded.sqljdbcschool.domain.model.Course;
import com.foxminded.sqljdbcschool.domain.model.Student;

public class CourseDistributor implements Distributor {

    private List<Student> students;
    private List<Course> courses;
    private Random random;

    public CourseDistributor(List<Student> students, List<Course> courses) {
        this.students = students;
        this.courses = courses;
        random = new Random();
    }

    @Override
    public void distribute() {
        if (students == null) {
            throw new IllegalArgumentException("Students list is null");
        }
        if (courses == null) {
            throw new IllegalArgumentException("Courses list is null");
        }
        students.forEach(student -> {
            int range = 1 + random.nextInt(3);
            Set<Course> coursesForSrudent = Stream.iterate(0, n -> n + 1)
                    .limit(range)
                    .map(n -> courses.get(random.nextInt(courses.size())))
                    .collect(Collectors.toSet());
            student.setCourses(coursesForSrudent);
        });
    }
}
