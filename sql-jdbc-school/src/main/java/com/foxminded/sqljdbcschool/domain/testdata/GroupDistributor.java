package com.foxminded.sqljdbcschool.domain.testdata;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import com.foxminded.sqljdbcschool.domain.model.Group;
import com.foxminded.sqljdbcschool.domain.model.Student;

public class GroupDistributor implements Distributor{

    private List<Student> students;
    private List<Group> groups;
    private Random random;

    public GroupDistributor(List<Student> students, List<Group> groups) {
        this.students = students;
        this.groups = groups;
        random = new Random();
    }

    @Override
    public void distribute() {
        if (students == null) {
            throw new IllegalArgumentException("Students list is null");
        }
        if (groups == null) {
            throw new IllegalArgumentException("Groups list is null");
        }
        students.forEach(student -> {
            int index = random.nextInt(groups.size());
            Group group = groups.get(index);
            List<Student> list = group.getStudents();
            if (list.size() < 30) {
                list.add(student);
                group.setStudents(list);
                student.setGroup(group);
                }
            });

        students.removeIf(student -> student.getGroup() == null);
        groups.forEach(group -> {
            List<Student> list = group.getStudents();
            if (list.size() < 10) {
                list.forEach(student -> students.remove(student));
                group.setStudents(Collections.emptyList());
            }
        });
    }
}
