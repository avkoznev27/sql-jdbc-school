package com.foxminded.sqljdbcschool.exceptions;

public class RequestHandlerException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public RequestHandlerException() {
    }

    public RequestHandlerException(String message) {
        super(message);
    }

    public RequestHandlerException(Throwable cause) {
        super(cause);
    }

    public RequestHandlerException(String message, Throwable cause) {
        super(message, cause);
    }

    public RequestHandlerException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
