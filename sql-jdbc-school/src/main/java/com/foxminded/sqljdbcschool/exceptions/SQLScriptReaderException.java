package com.foxminded.sqljdbcschool.exceptions;

public class SQLScriptReaderException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public SQLScriptReaderException() {
    }

    public SQLScriptReaderException(String message) {
        super(message);
    }

    public SQLScriptReaderException(Throwable cause) {
        super(cause);
    }

    public SQLScriptReaderException(String message, Throwable cause) {
        super(message, cause);
    }

    public SQLScriptReaderException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
