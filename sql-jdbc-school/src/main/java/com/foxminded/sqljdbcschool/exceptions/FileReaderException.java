package com.foxminded.sqljdbcschool.exceptions;

public class FileReaderException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public FileReaderException() {
    }

    public FileReaderException(String message) {
        super(message);
    }

    public FileReaderException(Throwable cause) {
        super(cause);
    }

    public FileReaderException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileReaderException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}