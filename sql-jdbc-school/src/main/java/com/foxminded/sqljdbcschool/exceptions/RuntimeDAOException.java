package com.foxminded.sqljdbcschool.exceptions;

public class RuntimeDAOException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public RuntimeDAOException() {
    }

    public RuntimeDAOException(String message) {
        super(message);
    }

    public RuntimeDAOException(Throwable cause) {
        super(cause);
    }

    public RuntimeDAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public RuntimeDAOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
