package com.foxminded.sqljdbcschool.exceptions;

public class PropertiesReaderException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public PropertiesReaderException() {
    }

    public PropertiesReaderException(String message) {
        super(message);
    }

    public PropertiesReaderException(Throwable cause) {
        super(cause);
    }

    public PropertiesReaderException(String message, Throwable cause) {
        super(message, cause);
    }

    public PropertiesReaderException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
