package com.foxminded.sqljdbcschool.exceptions;

public class TestDataLoaderException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public TestDataLoaderException() {
    }

    public TestDataLoaderException(String message) {
        super(message);
    }

    public TestDataLoaderException(Throwable cause) {
        super(cause);
    }

    public TestDataLoaderException(String message, Throwable cause) {
        super(message, cause);
    }

    public TestDataLoaderException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
