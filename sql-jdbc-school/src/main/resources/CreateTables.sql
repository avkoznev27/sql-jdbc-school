-- Table: public.groups
DROP TABLE IF EXISTS public.groups CASCADE;
CREATE TABLE public.groups
(
    group_id SERIAL NOT NULL,
    group_name character varying NOT NULL,
    CONSTRAINT groups_pkey PRIMARY KEY (group_id)
);

-- Table: public.students
DROP TABLE IF EXISTS public.students CASCADE;
CREATE TABLE public.students
(
    student_id SERIAL NOT NULL,
    group_id integer NOT NULL,
    first_name character varying NOT NULL,
    last_name character varying NOT NULL,
    CONSTRAINT students_pkey PRIMARY KEY (student_id),
    CONSTRAINT group_id FOREIGN KEY (group_id) REFERENCES public.groups (group_id)
);
-- Table: public.courses
DROP TABLE IF EXISTS public.courses CASCADE;
CREATE TABLE public.courses
(
    course_id SERIAL NOT NULL,
    course_name character varying NOT NULL,
    course_description text NOT NULL,
    CONSTRAINT courses_pkey PRIMARY KEY (course_id)
);
-- Table: public.students_courses
DROP TABLE IF EXISTS public.students_courses CASCADE;
CREATE TABLE public.students_courses
(
    student_id integer,
    course_id integer,
    CONSTRAINT course_id FOREIGN KEY (course_id) REFERENCES public.courses (course_id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT student_id FOREIGN KEY (student_id) REFERENCES public.students (student_id) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT student_course UNIQUE (student_id, course_id)
);
